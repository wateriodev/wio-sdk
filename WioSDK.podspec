Pod::Spec.new do |s|
    s.name             = 'WioSDK'
    s.version          = '1.0.1'
    s.summary          = 'A short  of WioSDK.'
    s.description      = 'A short  of WioSDK short  of WioSDK.'
    s.homepage         = 'https://water-io_ios@bitbucket.org/wateriodev/wiosdk.git'
    s.license          = { :type => 'MIT', :file => 'LICENSE' }
    s.author           = { 'israel@water-io.com' => 'israel@water-io.com' }
    s.source           = {
        :git => 'https://water-io_ios@bitbucket.org/wateriodev/wiosdk.git',
        :tag => s.version.to_s }
    
    
    s.ios.deployment_target = '10.3'
    s.source_files = ['WioSDK/Classes/**/*', 'Classes/*.{h,m}']
    s.swift_version = '4.2'
    
    s.dependency 'KeychainAccess', '~> 4.2.2'
    s.dependency 'CocoaLumberjack/Swift', '~> 3.7.0'

    s.static_framework = true
    
    s.vendored_frameworks = ['frameworks/WaterIOBleKit.framework', 'frameworks/WaterIONetwork.framework']
    
    #s.public_header_files = ['frameworks/WaterIOBleKit.framework/Headers/WaterIOBleKit.h',
    #                         'frameworks/WaterIONetwork.framework/Headers/WaterIONetwork.h']
    #s.ios.preserve_paths = ['WaterIOBleKit.framework', 'WaterIONetwork.framework']
    #s.frameworks = '[WaterIOBleKit.framework', 'WaterIONetwork.framework']
    #s.xcconfig = { 'FRAMEWORK_SEARCH_PATHS' => '/Applications/Xcode.app/Contents/Developer/Library/Frameworks' }
    #    s.ios.public_header_files = ['Frameworks/WaterIOBleKit.framework/Headers/*.h',
    #                                 'Frameworks/WaterIOBleKit.framework/Headers/*.h']
    
end
