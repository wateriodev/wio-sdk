# WioSDK

[License](https://img.shields.io/cocoapods/l/WioSDK.svg?style=flat)](https://cocoapods.org/pods/WioSDK)
[![Platform](https://img.shields.io/cocoapods/p/WioSDK.svg?style=flat)](https://cocoapods.org/pods/WioSDK)


## Requirements
Swift 5.0 or later
Xocde 12.0.1
WioSDK version 1.0.1

## Installation

WioSDK is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'WioSDK', :git => 'https://water-io_ios@bitbucket.org/wateriodev/wio-sdk.git'
```

For ios-13 and above add NSBluetoothAlwaysUsageDescription to info.plist

## Example

### App Launching
### Required to implement.

```swift
func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        WIOApp.shared.start(appToken: "", appType: "")
      return true
    }
}
```

###  Cap Connetion
```swift

import WioSDK

class CapConnectionViewController: UIViewController, WIOCapConnectionDelegate {

    lazy var capConnection: WIOCapConnection = {
        return WIOApp.shared.bleMenegaer.capConnection
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        capConnection.delegate = self
    }
    
    func start() {
        capConnection.startScan()
    }

    func stop() {
        capConnection.stopScan()
    }

    // MARK: - WIOCapConnectionDelegate
    func didUpdateConnection(_ isConnected: Bool) {
        
    }
    
    func didDeviceConnectionStateChange(state: WIOConnectionState) {
        
    }
    
    func didCapInfoUpdated(_ capInfo: WIOCapInfo) {
        
    }
}

```

### Get cap info
```swift
    WIOApp.shared.bleMenegaer.getMacaddress()
    WIOApp.shared.bleMenegaer.getCapVersion()
    WIOApp.shared.bleMenegaer.getCapUUID()
```

### Get cap events
```swift
class CapDataProcessor: UIViewController, WIOCapDataProcessorDelegate {
    
    lazy var capDataProcessor: WIOCapDataProcessor = {
        return WIOApp.shared.bleMenegaer.proseccor
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        capDataProcessor.delegate = self
    }
    
    //MARK: - WIOCapDataProcessorDelegate
    
    public func capDataModelDidUpdateEvents(events: [CapEvent]) {
        
    }
    
    public func didUpdateCapInfo(_ capVersion: String) {
        
    }
}
```

## License

WioSDK is available under the MIT license. See the LICENSE file for more info.


## Author
israel@water-io.com

## 
jazzy --build-tool-arguments -target,WioSDK   --output docs/swift_output
https://github.com/realm/jazzy


