//
//  NotificationService.swift
//  Wio-notifications
//
//  Created by israel water-io on 20/01/2021.
//  Copyright © 2021 CocoaPods. All rights reserved.
//

import UserNotifications

class NotificationService: UNNotificationServiceExtension {

    var contentHandler: ((UNNotificationContent) -> Void)?
    var bestAttemptContent: UNMutableNotificationContent?

    override func didReceive(_ request: UNNotificationRequest, withContentHandler contentHandler: @escaping (UNNotificationContent) -> Void) {
        self.contentHandler = contentHandler
        bestAttemptContent = (request.content.mutableCopy() as? UNMutableNotificationContent)
        
        if let bestAttemptContent = bestAttemptContent {
          
           
            if let sound = bestAttemptContent.userInfo["sound_url"] as? String {
                bestAttemptContent.sound = UNNotificationSound(named: UNNotificationSoundName(rawValue: sound))
            }
            
            if let threadID = bestAttemptContent.userInfo["thread_id"] as? String {
                bestAttemptContent.threadIdentifier = threadID
            }
            
            if let threadID = bestAttemptContent.userInfo["delete_notifications"] as? String {
                removeDeliveredNotification([threadID])
            }
            
            if let isDeleteAllNotificaitons = bestAttemptContent.userInfo["delete_old_notifications"] as? String {
                if isDeleteAllNotificaitons == "true" {
                    UNUserNotificationCenter.current().removeAllDeliveredNotifications()
                }
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                contentHandler(bestAttemptContent)
            }
        }
    }
    
    override func serviceExtensionTimeWillExpire() {
        if let contentHandler = contentHandler, let bestAttemptContent =  bestAttemptContent {
            contentHandler(bestAttemptContent)
        }
    }
    
    func removeDeliveredNotification() {
        UNUserNotificationCenter.current().removeAllDeliveredNotifications()
    }
    
    
    func removeDeliveredNotification(_ ids: [String]) {
        UNUserNotificationCenter.current().getDeliveredNotifications { notifications in
            for notificaiton in notifications {
                for id in ids {
                    if let deleteNotification = notificaiton.request.content.userInfo["notification_id"] as? String {
                        if deleteNotification == id {
                            DispatchQueue.main.async {
                                UNUserNotificationCenter.current()
                                    .removeDeliveredNotifications(withIdentifiers: [deleteNotification])
                            }
                            
                        }
                    }
                }
            }
        }
    }
}
