//
//  SettingsViewModel.swift
//  WioSDK_Example
//
//  Created by israel water-io on 25/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK
import WaterIOBleKit
import WaterIONetwork

class SettingsViewModel {
    
    let dataSource: [SettingItem] = [
        SettingItem(title: "Clear cache", value: "value"),
        SettingItem(title: "Send logs", value: "value"),
        SettingItem(title: "Notifications", value: "value"),
        SettingItem(title: "Beacon", value: "value"),
        SettingItem(title: "Folders", value: "value"),
        SettingItem(title: "Info", value: "value"),
    ]
    
    lazy var footerTitle: String = {
        let macAddress = CurrentDevice.currentDevice()?.MACAddress
        let capUUID = CurrentDevice.currentDevice()?.UUID
        let userId = UserCredentialStore().userCredentials?.userId
        let capVersion = CapInfo.currentCap()?.capVersion
        let tokenExpired =  getToken()
        let allUserInfo: [String] = [macAddress ?? "",
                             capUUID ?? "",
                             "\(userId ?? 0)",
                             capVersion ?? "",
                             tokenExpired ?? ""]
        
        //let deviceInfo = AttributesReport().toJSON().map{"\($0.key):\($0.value)"}
        //let alls = deviceInfo + all
        
        return allUserInfo.joined(separator: "\n")
    }()
    
    func sendLogs() {
        WIOApp.shared.showAllLogs()
    }
    
    func getToken() -> String?  {
        let token = UserCredentialStore().userCredentials?.accessToken ?? ""

        if UserDefaults.standard.integer(forKey: "token.expired") <= 0 {
            return nil
        }
        let timestamp = UserDefaults.standard.integer(forKey: "token.expired")/1000
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        let timeFormmat = DateFormatter()
        timeFormmat.timeStyle = .medium
        return "Token:" + token + "\n" + "Expired:" + timeFormmat.string(from: date)
    }
    
    func setToken() {
        let now = Date().adding(minutes: 2)
        UserDefaults.standard.set(Int(now.timeIntervalSince1970)*1000, forKey: "token.expired")
        UserDefaults.standard.synchronize()
    }
    
    func onClickRow(_ indexPath: IndexPath) {
        if indexPath.row == 5 {
            setToken()
        }
    }
}

