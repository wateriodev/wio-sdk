import UIKit

class SettingsViewController: UIViewController {
     @IBOutlet weak var tableView: UITableView!
    
    let viewModel = SettingsViewModel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

extension SettingsViewController: UITableViewDelegate, UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "settingID") else {
            fatalError()
        }
        cell.textLabel!.text = viewModel.dataSource[indexPath.row].title
        cell.detailTextLabel?.text = viewModel.dataSource[indexPath.row].value
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            Helper().clearCache()
        case 1:
            viewModel.sendLogs()
        case 2:
            openViewController("notificationsVC")
        case 3:
            openViewController("beaconVC")
        case 4:
            let viewController = BrowseFolderViewController()
            self.navigationController?.pushViewController(viewController, animated: true)
        case 5:
            let viewController = FileViewer(content: viewModel.footerTitle)
            self.navigationController?.pushViewController(viewController, animated: true)
        default:
            break
        }
        
        viewModel.onClickRow(indexPath)
    }
    
    func openViewController(_ name: String) {
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let viewController = storyboard.instantiateViewController(withIdentifier: name)
        self.navigationController?.pushViewController(viewController, animated: true)
    }
}
