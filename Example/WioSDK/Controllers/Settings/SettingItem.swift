import Foundation

struct SettingItem {
    let title: String
    let value: String
}
