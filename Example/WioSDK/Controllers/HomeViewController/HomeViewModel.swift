//
//  File.swift
//  WioSDK_Example
//
//  Created by israel water-io on 20/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK
import WioSDK
import WaterIOBleKit

typealias InfoDataCell = TableCellConfigurator<HomeTableViewCell, String>
typealias AdvtaiseCell = TableCellConfigurator<AdvtaiseTableViewCell, String>
typealias CapConnectCell = TableCellConfigurator<CapConnectTableViewCell, String>

final class HomeViewModel {
    var onDataSoueceDidUpdated:((IndexPath) -> Void) = { _ in }
    
    private var bleManager = WIOApp.shared.bleMenegaer
    private var networkService = WIONetwork()
    private var beaconController = WIOBeaconController.shared
    
    var tableSections: [HomeSection] = [
        HomeSection(rows: [CapConnectCell.init(item: "1")]),
        HomeSection(rows: [AdvtaiseCell.init(item: "2")]),
        HomeSection(rows: []),
    ]
    
    private lazy var beaconDescription: [String]? = {
        let description = UserDefaults.standard.array(forKey: "ble.beacon.description")
        return description as? [String]
    }()
    
    var isCapConnected: Bool {
        return bleManager?.capConnection.capUUID != nil
    }
    
    var capState: String {
        return bleManager?.capConnection.statusText ?? "UNKNOW"
    }

    func start() {
        bleManager?.processor.delegate = self
        bleManager?.capConnection.setCapVersion(.vitamin)
        networkService.start()
        beaconController.startMonitoring()
        beaconController.delegate = self
        networkService.wioAnalytics.startApp()
    }
    
    func numberOfSection() -> Int {
        return tableSections.count
    }
    
    func numberRowInSection(section: Int) -> Int {
        return tableSections[section].rows.count
    }
}

extension HomeViewModel: WIOCapDataProcessorDelegate {
    func capDataModelDidUpdateEvents(events: [CapEvent]) {
        for event in events {
            switch event.opCode {
            case .open:
                networkService.wioAnalytics.openCap(timestamp: Int(event.timestamp))
            case .close:
                networkService.wioAnalytics.closeCap(timestamp: Int(event.timestamp))
            default:
                break
            }
            insertEvent(name: "\(event.time) | \(event.opCodeName)")
        }
    }
    
    func didUpdateCapInfo(_ capVersion: String) {
        insertEvent(name: "capVersion \(capVersion)")
    }
    
    func insertEvent(name: String) {
        DispatchQueue.main.async {
            let cell = InfoDataCell.init(item: name)
            self.tableSections[2].rows.append(cell)
            self.onDataSoueceDidUpdated(IndexPath(row: 0, section: 2))
        }
    }
    
    func saveAdvertise(interval: Int, period: Int) {
        UserDefaults.standard.set(interval, forKey: "ble.inteval")
        UserDefaults.standard.set(period, forKey: "ble.period")
        UserDefaults.standard.synchronize()
    }
}


// MARK: Beacon
extension HomeViewModel: WIOBeaconControllerDelegate {
    func onBeaconDetected(_ state: WIOBeaconState, uuid: String) {
        let isBackground = UIApplication.shared.applicationState == .background
        let beaconEnterd = state == .inside
        
        let json: [String: Any] = [
            "event_time": Int(Date().timeIntervalSince1970),
            "event_type": "beacon_detected",
            "event_text": "beacon detected enter \(beaconEnterd) isBackground \(isBackground)",
        ]
        networkService.wioAnalytics.reportToUserlog(json: json)
        save(isEnter: beaconEnterd, time: Int(Date().timeIntervalSince1970))
    }
    
   
    
    func save(isEnter: Bool, time: Int) {
        let interval = UserDefaults.standard.integer(forKey: "ble.inteval")
        let period = UserDefaults.standard.integer(forKey: "ble.period")
        
        let timef = Date(timeIntervalSince1970: TimeInterval(time)).timeFromat
        
        var beacon = beaconDescription
        if beacon == nil {
            beacon = []
        }
        
        beacon?.append("interval: \(interval) period:\(period) isEnter:\(isEnter) time:\(timef)")
        
        UserDefaults.standard.set(beacon, forKey: "ble.beacon.description")
        UserDefaults.standard.synchronize()
    }
    
    func createGetRequest() {
    }
}
