//
//  CellConfigurator.swift
//  WioSDK_Example
//
//  Created by israel water-io on 30/09/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

protocol ConfigurableCell {
    associatedtype DataType
    func configure(data: DataType)
}

protocol CellConfigurator {
    static var reuseId: String { get }
    func configure(cell: UIView)
}

class TableCellConfigurator<CellType: ConfigurableCell, DataType>:
    CellConfigurator where CellType.DataType == DataType, CellType: UITableViewCell {

    static var reuseId: String { return String(describing: CellType.self) }

    let item: DataType

    init(item: DataType) {
        self.item = item
    }

    func configure(cell: UIView) {
        (cell as! CellType).configure(data: item)
    }
}
