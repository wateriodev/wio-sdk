//
//  HomeViewController.swift
//  WioSDK_Example
//
//  Created by israel water-io on 20/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
import CocoaLumberjack

class HomeViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    
    private let viewModel = HomeViewModel()
    
    private let network = NetworkTest()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.start()
        viewModel.onDataSoueceDidUpdated = updateTable
        
        UIDevice.current.isBatteryMonitoringEnabled = true
        let level = UIDevice.current.batteryLevel
        print(level)
        
        network.start()
    }
    
    func updateTable(indexPath: IndexPath) {
        //
        self.tableView.insertRows(at: [indexPath], with: .automatic)
        self.tableView.reloadData()
    }
}

extension HomeViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSection()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberRowInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let section = viewModel.tableSections[indexPath.section]
        let item = section.rows[indexPath.row]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: type(of: item).reuseId)!
        cell.selectionStyle = .none
        item.configure(cell: cell)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = CapConnectionViewController()
        let nvc = UINavigationController(rootViewController: vc)
        self.navigationController?.present(nvc, animated: true, completion: nil)
    }
}
//
//extension HomeViewController: AdvtaiseTableViewCellDelegate {
//    func onClickSubmit(interval: Int, period: Int) {
//        DDLogInfo("<========= interval:\(interval) period:\(period) =======>")
//        viewModel.saveAdvertise(interval: interval, period: period)
//    }
//}
//
//extension HomeViewController: CapConnectTableViewCellDelegate {
//    func onClickButton() {
//        //bleManager?.capConnection.waitToCheckIsMyCap = true
//        let vc = CapConnectionViewController()
//        let nvc = UINavigationController(rootViewController: vc)
//        self.navigationController?.present(nvc, animated: true, completion: nil)
//    }
//}
