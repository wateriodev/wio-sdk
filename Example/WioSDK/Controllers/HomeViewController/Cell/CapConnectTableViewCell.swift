//
//  CapConnectTableViewCell.swift
//  WioSDK_Example
//
//  Created by israel water-io on 19/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

protocol CapConnectTableViewCellDelegate: class {
    func onClickButton()
}

class CapConnectTableViewCell: UITableViewCell, ConfigurableCell {
    
    weak var delegate: CapConnectTableViewCellDelegate?

    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var connectButton: UIButton!
    
    @IBAction func onClickConnect(_ sender: UIButton) {
        delegate?.onClickButton()
    }
    
    func configure(data title: String) {
        self.statusLabel.text = title
        //self.connectButton.setTitle(buttonTitle, for: .normal)
    }
}
