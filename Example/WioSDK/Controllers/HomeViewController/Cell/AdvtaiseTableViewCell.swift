//
//  AdvtaiseTableViewCell.swift
//  WioSDK_Example
//
//  Created by israel water-io on 19/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit
protocol AdvtaiseTableViewCellDelegate: class {
    func onClickSubmit(interval: Int, period: Int)
}

class AdvtaiseTableViewCell: UITableViewCell, ConfigurableCell {
    
    weak var delegate: AdvtaiseTableViewCellDelegate?
    
    @IBOutlet weak var intervalLabel: UILabel!
    @IBOutlet weak var periodLabel: UILabel!
    
    @IBOutlet weak var intervalTextFiled: UITextField!
    @IBOutlet weak var periodTextFiled: UITextField!
    
    @IBAction func didClickSubmit(_ sender: Any) {
        
        guard let interval = Int(intervalTextFiled.text ?? ""),
              let period = Int(periodTextFiled.text ?? "") else {
            return
        }
        delegate?.onClickSubmit(interval: interval, period: period)
    }
    
    func configure(data title: String) {
        self.intervalLabel?.text = title
        self.periodLabel?.text = title
    }
}
