//
//  HomeTableViewCell.swift
//  WioSDK_Example
//
//  Created by israel water-io on 30/09/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

class HomeTableViewCell: UITableViewCell, ConfigurableCell {

    func configure(data text: String) {
        self.textLabel?.text = text
    }
}
