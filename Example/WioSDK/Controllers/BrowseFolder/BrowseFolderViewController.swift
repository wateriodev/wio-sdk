//
//  BrowseFolderViewController.swift
//  WioSDK_Example
//
//  Created by israel water-io on 25/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

struct File {
    let name: String
    let type: String
    let path: URL
    let isFile: Bool
    
    init(path: URL) {
        self.path = path
        self.type = path.pathExtension
        self.name = path.lastPathComponent
        self.isFile = !path.hasDirectoryPath
    }
}


class BrowseFolderViewController: UITableViewController {
    
    private var files: [File]?
    private var root: URL
    
    init(root: URL? = nil) {
        if let root = root {
            self.root = root
        } else {
            self.root = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        }
        super.init(style: .plain)
        
        if let folders = findFileInFolder(self.root) {
            self.files = folders.map{File(path: $0)}
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "folderCell")
    }
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return files?.count ?? 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = UITableViewCell(style: .default, reuseIdentifier: "folderCell")
        cell.textLabel?.numberOfLines = 0
        cell.textLabel?.text = files?[indexPath.row].name
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        guard let path = self.files?[indexPath.row].path else {
            return
        }
        
        
        if self.files?[indexPath.row].isFile == true {
            openFile(path: path)
            return
        } else {
            let viewController = BrowseFolderViewController(root: path)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
    
    func openFile(path: URL) {
        if let content = try? String(contentsOf: path, encoding: .utf8) {
            let viewController = FileViewer(content: content)
            self.navigationController?.pushViewController(viewController, animated: true)
        }
    }
}

extension BrowseFolderViewController {
    func findFileInFolder(_ url: URL) -> [URL]? {
        do {
            let directoryContents = try FileManager.default.contentsOfDirectory(at: url,
                                                                                includingPropertiesForKeys: nil,
                                                                                options: [])
            return directoryContents
        } catch {
            return nil
        }
    }
}


class FileViewer: UIViewController {
    let content: String

    lazy var text: UITextView = {
        let label = UITextView(frame: CGRect(x: 10,
                                          y: 10,
                                          width: self.view.frame.size.width - 20,
                                          height: self.view.frame.size.height - 20))
        //label.numberOfLines = 0
        return label
    }()
    
    init(content: String) {
        self.content = content
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
   
    override func viewDidLoad() {
        self.view.backgroundColor = .white
        self.text.text = content
        text.isEditable = false
        self.view.addSubview(text)
    }
}
