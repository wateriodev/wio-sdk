//
//  BeaconViewController.swift
//  WioSDK_Example
//
//  Created by israel water-io on 19/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

class BeaconViewController: UIViewController {
     @IBOutlet weak var tableView: UITableView!
    
    var dataSource: [String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let actions = UserDefaults.standard.array(forKey: "ble.beacon.description") as? [String] {
            self.dataSource = actions
            self.tableView.reloadData()
        }
    }
}

extension BeaconViewController: UITableViewDelegate, UITableViewDataSource {
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard  let cell = tableView.dequeueReusableCell(withIdentifier: "beaconCell") else {
            fatalError()
        }
        cell.textLabel!.text = dataSource[indexPath.row]
        return cell
    }
}
