//
//  NotificaionsViewController.swift
//  WioSDK_Example
//
//  Created by israel water-io on 23/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import UIKit

class NotificationsViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    private var dataSource: [String]  = []
    
    var currntDay = Date()//.adding(minutes: 700)
    private let weeklyNotifications = WeeklyNotification()
    
    var notificatoins1 = [
        DoseNotification(hour: 08, minute: 00, title: "Hey", body: ["Body"]),
        DoseNotification(hour: 17, minute: 00, title: "Hey", body: ["Body"]),
        DoseNotification(hour: 20, minute: 00, title: "Hey", body: ["Body"]),
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()
        loadAllData()
    }
    
    func loadAllData() {
        dataSource = []
        LocalNotificaionsManager().getPendingNotificationRequests {  [weak self] notifications in
                 DispatchQueue.main.async {
                     self?.notificationsDidLoaded(notifications: notifications)
                 }
             }
    }
    
    func notificationsDidLoaded(notifications: [UNNotificationRequest]) {
        
        let notificationsSorted = notifications.sorted{ a, b in
            let triggerA = a.trigger  as? UNCalendarNotificationTrigger
            let triggerB = b.trigger as? UNCalendarNotificationTrigger
            return triggerA!.nextTriggerDate()! < triggerB!.nextTriggerDate()!
        }
        
        self.dataSource.append("Date: \(currntDay.dateFormat)")
        for notification in notificationsSorted {
           let trigger = notification.trigger as? UNCalendarNotificationTrigger
            let day =  "\(trigger?.nextTriggerDate()?.dateFormat ?? "")"
            let repeats = "Repeats:\(notification.trigger?.repeats ?? false)"
            let id = "ID:\(notification.identifier)"
            let full = [day, repeats, id].joined(separator: " ")
            self.dataSource.append(full)
        }
                 
        self.tableView.reloadData()
    }
   
    
    @IBAction func cancellAll(_ sender: Any) {
        LocalNotificaionsManager().cancelAllNotifications()
    }
    
    @IBAction func setupNotification(_ sender: Any) {
        WeeklyNotification().setupNotification(reminders: notificatoins1)
    }
    
    @IBAction func delayNotification(_ sender: Any) {
        WeeklyNotification().delayNotificationToNextWeekByDay(reminder: notificatoins1[1], day: currntDay)
    }
    
    @IBAction func fixNotification(_ sender: Any) {
       weeklyNotifications.fixNoifications(currntDate: currntDay)
    }
}


extension NotificationsViewController: UITableViewDelegate, UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return dataSource.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "notificationCell") else {
            fatalError()
        }
        
        cell.textLabel?.text = dataSource[indexPath.row]
        return cell
    }

}
