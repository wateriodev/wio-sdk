import UIKit
import WioSDK

public class CapConnectionViewController: UIViewController {
    
    @IBOutlet var stackView: UIStackView!
    @IBOutlet var statusLabel: UILabel!
    @IBOutlet var loadingSpinnerContainerView: UIView!
    
    // MARK: Private variables
    
    private lazy var doneButton: UIButton = {
        var doneButton = UIButton()
        doneButton.setTitle("Done", for: .normal)
        doneButton.addTarget(self, action: #selector(doneButtonPressed), for: .touchUpInside)
        doneButton.setTitleColor(.black, for: .normal)
        doneButton.layer.borderColor = UIColor.black.cgColor
        doneButton.layer.borderWidth = 1
        return doneButton
    }()
    
    private lazy var forgetThisCapButton: UIButton = {
        var forgetThisCapButton = UIButton()
        forgetThisCapButton.setTitle("Forget This Cap", for: .normal)
        forgetThisCapButton.addTarget(self, action: #selector(promptToForgetCap), for: .touchUpInside)
        forgetThisCapButton.setTitleColor(.black, for: .normal)
        forgetThisCapButton.layer.borderColor = UIColor.black.cgColor
        forgetThisCapButton.layer.borderWidth = 1
        return forgetThisCapButton
    }()
    
    private lazy var connectToCapButton: UIButton = {
        var connectToCapButton = UIButton()
        connectToCapButton.setTitle("Connect", for: .normal)
        connectToCapButton.addTarget(self, action: #selector(scan), for: .touchUpInside)
        connectToCapButton.setTitleColor(.black, for: .normal)
        connectToCapButton.layer.borderColor = UIColor.black.cgColor
        connectToCapButton.layer.borderWidth = 1
        return connectToCapButton
    }()
    
    private let capConnection: WIOCapConnection
    
    // MARK: -
    
    public init() {
        self.capConnection = WIOApp.shared.bleMenegaer.capConnection
        super.init(nibName: "CapConnectionViewController", bundle: Bundle(for:CapConnectionViewController.self))
        self.capConnection.delegate = self
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override public func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override public func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Connect To Cap"
        
        navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done, target: self, action: #selector(doneButtonPressed))
        applyTheme()
        updateState()
    }
    
    private func prepareForUpdate() {
        // remove all arranged views from stack view except for the first one (status label)
        if stackView.arrangedSubviews.count > 1 {
            let views = stackView.arrangedSubviews[1...]
            views.forEach {
                $0.removeFromSuperview()
            }
        }
    }
    
    private func updateState() {
        statusLabel.text = capConnection.statusText
        
        prepareForUpdate()
        
        if case .connected(let newDevice) = capConnection.state {
            if newDevice {
                stackView.addArrangedSubview(doneButton)
            } else {
                stackView.addArrangedSubview(forgetThisCapButton)
            }
        } else if case .scanning = capConnection.state {
            // TODO: add list of caps near by or just a loadingNode for now.
            // if only one device discovered -> connect, otherwise show options?
            
            stackView.addArrangedSubview(loadingSpinnerContainerView)
        } else {
            stackView.addArrangedSubview(connectToCapButton)
        }
    }
    
    private func applyTheme() {
        //statusLabel.font = UIFont(name: Style.font.defaultFont, size: 18)
        statusLabel.textColor = .black
    }
    
    //MARK: Action
    
    @objc func scan() {
        capConnection.waitToCheckIsMyCap = true
        WIOApp.shared.bleMenegaer.capConnection.startScan()
        capConnection.startScan()
    }
    
    @objc func doneButtonPressed() {
        navigationController?.dismiss(animated: true, completion: nil)
        //self.doneButtonPressed()
        capConnection.waitToCheckIsMyCap = false
    }
    
    @objc private func promptToForgetCap() {
        let title: String? = nil
        let message = NSLocalizedString("Are you sure you want to forget device?", comment: "")
        let alertController = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: NSLocalizedString("Cancel", comment: "alert.cancel"), style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: NSLocalizedString("Yes", comment: "alert.yes"), style: .default) { [weak self] (action) in
            self?.forgetCap()
        }
        
        alertController.addAction(openAction)
        present(alertController, animated: true, completion: nil)
    }
    
    func forgetCap() {
        capConnection.forgetCap()
    }
}

extension CapConnectionViewController: WIOCapConnectionDelegate {
    public func didDeviceConnectionStateChange(state: WIOConnectionState) {
//        switch state {
//        case .connected(let isANew):
//            if isANew {
//                DispatchQueue.main.asyncAfter(deadline: .now() + 4) {
//                    self.capConnection.waitToCheckIsMyCap = false
//                }
//            }
//        default:
//            break
//        }
        updateState()
    }
    
    public func didUpdateConnection(_ isConnected: Bool) {
        
    }
    
    public func didCapInfoUpdated(_ capInfo: WIOCapInfo) {
        print("cap Info updated\( capInfo)")
    }
}
