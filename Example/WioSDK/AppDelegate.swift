//
//  AppDelegate.swift
//  WioSDK
//
//  Created by israel@water-io.com on 07/29/2019.
//  Copyright (c) 2019 israel@water-io.com. All rights reserved.
//

import UIKit
import WioSDK
import CocoaLumberjack

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, UNUserNotificationCenterDelegate {
    
    var window: UIWindow?
    
    private var notificationService: WIONotificationsManager?
    
    //var backgourdTask = BackgourndTask()
    
    
    func setupNotificationCategory() {
        let content = UNMutableNotificationContent()
        content.title = "Notifications Group"
        content.body = "Tutorial by Ashish Kakkad"
        content.threadIdentifier = "notify-team-ios"
    }
    
    
    func application(_ application: UIApplication, willFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        WIOApp.shared.isDebugMode = true
        WIOApp.shared.start(appToken: Configuration.appToken,
                            appType: Configuration.appType)
        
        UIApplication.shared.setMinimumBackgroundFetchInterval(UIApplication.backgroundFetchIntervalMinimum)
        
        if let option = launchOptions?[.bluetoothCentrals] {
            DDLogInfo("App was background launched. bluetoothCentrals: \(option)")
        }
        
        
        //backgourdTask.start()
        return true
    }
    
    
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey : Any]? = nil) -> Bool {
        
        notificationService = WIONotificationsManager(networkManager: WIOApp.shared.networkManager,
                                                      configuration: PushConfiguration (appType: Configuration.appType, pushServerKey: Configuration.pushServerKey, hostBundleId: Configuration.appBundleId), isSendbox: true)
        notificationService?.start()
        return true
    }
    
    
    func applicationDidBecomeActive(_ application: UIApplication) {
        UIApplication.shared.applicationIconBadgeNumber = 0
    }
    
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let token = deviceToken.map { String(format: "%02.2hhx", $0) }.joined()
        print("Device token is: \(token)")
        notificationService?.onDidRegisterForRemoteNotifications(withDeviceToken: deviceToken)
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
          
            completionHandler(.newData)
    }
    
    func applicationDidEnterBackground(_ application: UIApplication) {
        let ti = UIApplication.shared.backgroundTimeRemaining
        DDLogInfo("backgroundTimeRemaining: \(ti)") // just for debug
        
        
        //        if #available(iOS 13.0, *) {
        //            backgourdTask.scheduleProcessingTask()
        //            backgourdTask.scheduleAppRefreshTask()
        //        }
    }
    
    
    func applicationWillTerminate(_ application: UIApplication) {
        //        if #available(iOS 13.0, *) {
        //            backgourdTask.scheduleProcessingTask()
        //            backgourdTask.scheduleAppRefreshTask()
        //        }
    }

}
