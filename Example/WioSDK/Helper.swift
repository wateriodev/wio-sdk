//
//  Helper.swift
//  WioSDK_Example
//
//  Created by israel water-io on 19/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK

class Helper {
    
    func clearCache() {
        let cacheURL =  FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first!
        let fileManager = FileManager.default
        do {
            // Get the directory contents urls (including subfolders urls)
            let directoryContents = try FileManager.default.contentsOfDirectory( at: cacheURL, includingPropertiesForKeys: nil, options: [])
            for file in directoryContents {
                do {
                    try fileManager.removeItem(at: file)
                }
                catch let error as NSError {
                    debugPrint("Ooops! Something went wrong: \(error)")
                }

            }
        } catch let error as NSError {
            print(error.localizedDescription)
        }
    }
}

extension Date {
    var timeFromat: String {
        let foramt = DateFormatter()
        foramt.dateFormat = "HH:mm:ss"
        //let date = Date(timeIntervalSince1970: TimeInterval(self.timestamp))
        return foramt.string(from: self)
    }
    
    var dayFromat: String {
        let foramt = DateFormatter()
        foramt.dateFormat = "MM/dd"
        return foramt.string(from: self)
    }
    
    var dateFormat: String {
        let foramt = DateFormatter()
        foramt.dateFormat = "MM/dd HH:mm"
        //foramt.timeStyle = .medium
        return foramt.string(from: self)
    }
    
    var fullDateFormat: String {
        let foramt = DateFormatter()
        foramt.dateFormat = "yyyy/MM/dd HH:mm"
        //foramt.timeStyle = .medium
        return foramt.string(from: self)
    }

}
extension CapEvent {
    
    var time: String {
        let foramt = DateFormatter()
        foramt.dateFormat = "hh:mm:ss"
        let date = Date(timeIntervalSince1970: TimeInterval(self.timestamp))
        return foramt.string(from: date)
    }
    var opCodeName: String {
        switch opCode {
        case .pair:
            return "pair"
        case .preRemind:
            return "preRemind"
        case .remind:
            return "remind"
        case .open:
            return "open"
        case .close:
            return "close"
        case .appConnected:
            return "appConnected"
        case .appDisconnected:
            return "appDisconnected"
        case .unknown:
            return "unknown"
        case .length:
            return "length"
        case .measure:
            return "measure"
        default:
            return ""
        }
    }
}
