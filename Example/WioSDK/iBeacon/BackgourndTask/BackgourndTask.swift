import Foundation
import BackgroundTasks

class BackgourndTask {
    private let refreshTaskID = "com.wio.appRefresh"
    private let proccessTaskID = "com.wio.appProccess"
    
    func start() {
        if #available(iOS 13.0, *) {
            registerProcessingTask()
            registerAppRefreshTask()
        }
    }
    
    @available(iOS 13.0, *)
    func registerAppRefreshTask() {
        let res = BGTaskScheduler.shared.register(
            forTaskWithIdentifier: refreshTaskID,
            using: nil,
            launchHandler: { task in
                self.handleAppRefreshTask(task)
        })
        if !res {
            print("failed registering \(refreshTaskID)")
        }
    }
    
    @available(iOS 13.0, *)
    func registerProcessingTask() {
        let res = BGTaskScheduler.shared.register(
            forTaskWithIdentifier: proccessTaskID,
            using: nil,
            launchHandler: { task in
                self.handleProcessingTask(task)
        })
        if !res {
            print("failed registering \(proccessTaskID)")
        }
    }
    
    
    
    @available(iOS 13.0, *)
    func setupProcess() {
        let res = BGTaskScheduler.shared.register(
            forTaskWithIdentifier: proccessTaskID,
            using: nil,
            launchHandler: { task in
                self.handleProcessingTask(task)
        })
        if !res {
            print("failed registering \(proccessTaskID)")
        }
    }
    
    @available(iOS 13.0, *)
    private func handleProcessingTask(_ task: BGTask) {
        scheduleProcessingTask()
        
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1;
        
        task.expirationHandler = {
            queue.cancelAllOperations()
        }
        
        
        if let last = queue.operations.last {
            last.completionBlock = {
                task.setTaskCompleted(success: true)
            }
        }
        
        
        queue.addOperation({
            //SpecialBleManager.shared().keepAliveBLEStart(forTask: "BGProccessTask")
        })
    }
    
    @available(iOS 13.0, *)
    func scheduleProcessingTask() {
        let error: Error? = nil
        BGTaskScheduler.shared.cancel(taskRequestWithIdentifier: proccessTaskID)
        let request = BGProcessingTaskRequest(identifier: proccessTaskID)
        request.requiresNetworkConnectivity = false
        request.requiresExternalPower = false
        request.earliestBeginDate = Date(timeIntervalSinceNow: 10 * 60)
        var success = false
        do {
            try BGTaskScheduler.shared.submit(request)
            success = true
        } catch {
        }
        if !success {
            if let error = error {
                print("Failed to submit proccess request: \(error)")
            }
        } else {
            print("Success submit proccess request \(request)")
        }
    }
    
    @available(iOS 13.0, *)
    func scheduleAppRefreshTask() {
        let error: Error? = nil
        BGTaskScheduler.shared.cancel(taskRequestWithIdentifier: refreshTaskID)
        let request = BGAppRefreshTaskRequest(identifier: refreshTaskID)
        request.earliestBeginDate = Date(timeIntervalSinceNow: 15 * 60)
        var success = false
        do {
            try BGTaskScheduler.shared.submit(request)
            success = true
        } catch {
        }
        if !success {
            if let error = error {
                print("Failed to submit refresh request: \(error)")
            }
        } else {
            print("Success submit refresh request \(request)")
        }
    }
    
    @available(iOS 13.0, *)
    func handleAppRefreshTask(_ task: BGTask) {
        scheduleAppRefreshTask()
        
        let queue = OperationQueue()
        queue.maxConcurrentOperationCount = 1
        
        task.expirationHandler = {
            queue.cancelAllOperations()
        }
        
        let last = queue.operations.last
        if last != nil {
            last?.completionBlock = {
                task.setTaskCompleted(success: true)
            }
        }
        
        queue.addOperation({
            //SpecialBleManager.shared().keepAliveBLEStart(forTask: "BGRefreshTask")
        })
    }
}
