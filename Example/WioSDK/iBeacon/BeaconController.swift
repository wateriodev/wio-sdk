//
//  BeaconController.swift
//  WaterIOBleKit
//
//  Created by israel on 23/04/2018.
//  Copyright © 2018 WaterIO. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import CocoaLumberjack
import WioSDK

enum BeaconState {
    case inside, outside, unknown
}

protocol BeaconControllerDelegate: class {
    func onBeaconDetected(_ state: BeaconState, uuid: String)
}

class BeaconController: NSObject {
    
    private var locationManager = CLLocationManager()
    public weak var delegate: BeaconControllerDelegate?
    
    
    public var isMonitoring: Bool {
        return locationManager.allowsBackgroundLocationUpdates
    }
    
    private var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    lazy var beaconRegion: CLBeaconRegion = {
        let beacon = CLBeaconRegion(proximityUUID: IBeacon.proximityUUID,
                                     major: IBeacon.major,
                                     minor: IBeacon.minor,
                                     identifier: IBeacon.beaconID)
        beacon.notifyEntryStateOnDisplay = true
        beacon.notifyOnExit = true
        beacon.notifyOnEntry = true
        return beacon
    }()
    
    func startMonitoring() {
        locationManager.delegate = self
        locationManager.startMonitoring(for: beaconRegion)
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = .greatestFiniteMagnitude
        locationManager.requestAlwaysAuthorization()
    }
    
    func stopMonitoring() {
        locationManager.stopMonitoring(for: beaconRegion)
    }
}

// MARK: CLLocationManagerDelegate
extension BeaconController: CLLocationManagerDelegate {
        func locationManager(_ manager: CLLocationManager,
                             didRangeBeacons beacons: [CLBeacon],
                             in region: CLBeaconRegion) {
            
            for beacon in beacons {
                sendNotification("locationManager didRangeBeacons for \(beacon.proximity)")
            }
        }
    
    func locationManager(_ manager: CLLocationManager, didDetermineState state: CLRegionState, for region: CLRegion) {
        var newState: BeaconState = .unknown
        switch state {
        case .inside:
            sendNotification("locationManager didDetermineState INSIDE for \(region.identifier)")
            newState = .inside
        case .outside:
            sendNotification("locationManager didDetermineState outside for \(region.identifier)")
            newState = .outside
        case .unknown:
            sendNotification("locationManager didDetermineState unknown for \(region.identifier)")
            newState = .unknown
        }
        
        self.delegate?.onBeaconDetected(newState, uuid: region.identifier)
    }
    
    func locationManager(_ manager: CLLocationManager, didEnterRegion region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didEnterRegion \(region)")
        sendNotification("Enter Region")
    }
    
    func locationManager(_ manager: CLLocationManager, didExitRegion region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didExitRegion \(region)")
        sendNotification("Exit Region")
    }
    
    func locationManager(_ manager: CLLocationManager, didStartMonitoringFor region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didStartMonitoringFor \(region)")
        sendNotification("Start Monitoring")
    }
    
    func locationManager(_ manager: CLLocationManager, monitoringDidFailFor region: CLRegion?, withError error: Error) {
        DDLogInfo("locationManager monitoringDidFailFor \(region!)")
    }
    
    public func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        DDLogInfo("locationManager monitoringDidFailFor \(manager)")
    }
}

extension BeaconController {
    func sendNotification(_ body: String) {
        DDLogInfo(body)
        
        WIOLocalNotification().scheduleNotification(notification: WIONotification(identifier: "123",
                          alertTitle: "iBeacon",
                          alertBody: body,
                          date: Date().addingTimeInterval(5),
                          repeats: .none))
    }
}
