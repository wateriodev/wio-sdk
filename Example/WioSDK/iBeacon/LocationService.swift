////
////  LocationService.swift
////  WioSDK_Example
////
////  Created by israel water-io on 30/07/2020.
////  Copyright © 2020 CocoaPods. All rights reserved.
////
//
//import Foundation
//import CoreLocation
//
//public enum LocationType {
//    case always
//    case WhenInUse
//}
//
//final class LocationService: NSObject {
//    
//    fileprivate var locationManager: CLLocationManager?
//    
//    fileprivate var currentUserLocation: CLLocationCoordinate2D?
//    
//    var locationServiceDidGetLocation: ((Error?) -> Void)?
//    
//    var locationType: LocationType = .WhenInUse
//    
//    
//    var locationServicesEnabled: Bool {
//        return CLLocationManager.locationServicesEnabled()
//    }
//    
//    var authorizationStatus: CLAuthorizationStatus {
//       return CLLocationManager.authorizationStatus()
//    }
//    
//    func requestUserLocation(locationType: LocationType) {
//        if locationServicesEnabled == false {
//            return
//        }
//        
//        if locationManager == nil {
//            locationManager = CLLocationManager()
//            locationManager?.delegate = self
//            locationManager?.desiredAccuracy = kCLLocationAccuracyThreeKilometers
//        }
//        requestLocation(locationType: locationType)
//    }
//    
//    fileprivate func requestLocation(locationType: LocationType) {
//        switch locationType {
//        case .always:
//            locationManager?.requestAlwaysAuthorization()
//        case .WhenInUse:
//            locationManager?.requestWhenInUseAuthorization()
//        }
//    }
//    
//    fileprivate func notifyCompletion(with error: Error?) {
//        locationServiceDidGetLocation?(error)
//        locationServiceDidGetLocation = nil
//    }
//}
//
//// MARK: - CLLocationManagerDelegate
//extension LocationService: CLLocationManagerDelegate {
//    
//    func locationManager(_ manager: CLLocationManager,
//                         didChangeAuthorization status: CLAuthorizationStatus) {
//        
//        switch status {
//        case .denied:
//            break
//        case .authorizedAlways:
//            break
//        case .authorizedWhenInUse:
//            break
//        case .notDetermined:
//            break
//        case .restricted:
//            break
//        default:
//            break
//        }
//    }
//    
//    func locationManager(_ manager: CLLocationManager,
//                         didUpdateLocations locations: [CLLocation]) {
//        if locationType == .WhenInUse {
//            manager.delegate = nil
//        }
//        
////        guard let loc = locations.first?.coordinate else {
////            return
////        }
//        
//        notifyCompletion(with: nil)
//    }
//    
//    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
//        manager.delegate = nil
//    }
//}
