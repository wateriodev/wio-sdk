//
//  Configuration.swift
//  WioSDK_Example
//
//  Created by israel on 06/02/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import UIKit


struct BottleType {
    let distances: [Int]
    let measures: [Double]
}

//
//let convertToMl = ConvertMilimeterUtil.getMeasure(distance: Int(value),
//                                             distances: selectedBottle.distances,
//                                             measures: selectedBottle.measures)


enum Configuration {
    static let email = "1221\(UIDevice.current.identifierForVendor!.uuidString)@demo.com"
    static let password = "Israel123"
    static let appName = "waterio-2020"
    static let appType = "waterio-2020"
    static let appToken = "b7X3UHW_PiEM7jGOac7pXtJHBTONxHBfjUDQsaIoFe0BAXSnKO2LzQscETGsOMKMQQzT9V"
    static let pushAppID = "1:646532875699:ios:b2e8cc5b42c33b2eb7f7c6"
    static let pushApiKey = "AIzaSyC1x92HNz42qUR9D4h4whn4m7AUedmFb-4"
    static let appBundleId = "com.waterio.vitamins.ios"
    static let pushServerKey = "AAAA5YYMfTY:APA91bHY6KS75hWFv_LDO9QdsyLRj83NoWZ4AfSziZ0hp1qIrEDF2UoJlO8UaYlVxhAUVNnarm5bGZl18nfe35ZvSArsmnqcSAZwxPlnopYI6WvKaMnhZX7RGtwcuqyAZogLjh52WpQi"
    
//    static let appType = "vitamins-io"
//    static let appToken = "fbAbHYhloqeqO0VXLxHYCgBdVBqDNOFWz1jMUu0zyTGeYfunjLSYQOPrXQhw4FNMBeFENi"
//    static let pushServerKey = "AAAA5YYMfTY:APA91bHY6KS75hWFv_LDO9QdsyLRj83NoWZ4AfSziZ0hp1qIrEDF2UoJlO8UaYlVxhAUVNnarm5bGZl18nfe35ZvSArsmnqcSAZwxPlnopYI6WvKaMnhZX7RGtwcuqyAZogLjh52WpQi"
//    static let appBundleId = "com.waterio.vitamins.ios"
    
//    
//    static let appType = "wrapper-gatorade"
//    static let appToken = "cNk2WajLLfDn49f300BAe5EB5XUPRcyjnjLIfdRTVyRvVpRM4uos2XTcec0bBI4B3vUjBA-access"
//    static let pushServerKey = "AAAA5YYMfTY:APA91bHY6KS75hWFv_LDO9QdsyLRj83NoWZ4AfSziZ0hp1qIrEDF2UoJlO8UaYlVxhAUVNnarm5bGZl18nfe35ZvSArsmnqcSAZwxPlnopYI6WvKaMnhZX7RGtwcuqyAZogLjh52WpQi"
//    static let appBundleId = "com.waterio.vitamins.ios"

    
    static let currntBottle = BottleType(distances:
        //[252, 248, 245, 238, 231, 219, 204, 188, 167, 143, 120, 104, 85, 72, 57, 47, 37, 26],
          [26,37,47,57,72,85,104,120,143,167,188,204,219,231,238,245,248,252],
                                         measures:
        [850, 800, 750, 700, 650, 600, 550, 500, 450, 400, 350, 300, 250, 200, 150, 100, 50, 0])
    
}
