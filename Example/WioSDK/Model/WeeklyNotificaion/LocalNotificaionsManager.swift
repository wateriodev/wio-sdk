//
//  AppNotifications.swift
//  Runner
//
//  Created by israel on 08/12/2019.
//  Copyright © 2019 The Chromium Authors. All rights reserved.
//

import Foundation
import WioSDK

//MARK: Notificaiton
struct LocalNotificaionsManager {
    
    func scheduleLocalNotification(id: String, title: String, body: String, date: Date, category: String? = nil) {
        let scheduler = WIOLocalNotification()
        let firstNotification = WIONotification(identifier: id,
                                                alertTitle: title,
                                                alertBody: body,
                                                date: date, repeats: .none)
        if let category = category {
            firstNotification.category = category
        }
        scheduler.scheduleNotification(notification: firstNotification)
    }
    
    func scheduleLocalNotification(id: String, title: String, body: String, date: Date, repeats: RepeatingInterval = RepeatingInterval.none, category: String? = nil) {
        let scheduler = WIOLocalNotification()
        let firstNotification = WIONotification(identifier: id,
                                                alertTitle: title,
                                                alertBody: body,
                                                date: date,
                                                repeats: repeats)
        if let category = category {
                   firstNotification.category = category
               }
        
        scheduler.scheduleNotification(notification: firstNotification)
        setupYesNoCategory(scheduler: scheduler, id: id)
    }
    
    func scheduleLocalNotificationWithTimeInterval(id: String, title: String, body: String, timeInterval: TimeInterval, category: String? = nil) {
        let scheduler = WIOLocalNotification()
        let firstNotification = WIONotification(identifier: id,
                                                alertTitle: title,
                                                alertBody: body,
                                                date: Date(),
                                                repeats: .none)
        if let category = category {
                   firstNotification.category = category
               }
        
        scheduler.scheduleNotificationTimeInterval(notification: firstNotification, timeInterval: timeInterval)
        setupYesNoCategory(scheduler: scheduler, id: id)
    }
    
    func setupYesNoCategory(scheduler: WIOLocalNotification, id: String) {
        let okButtonId = "push.action.ok"
        let cancelButtonId =  "push.action.cancel"
        let category = WIONotificationCategory(categoryIdentifier: id)
        category.addActionButton(identifier: okButtonId, title: "Yes")
        category.addActionButton(identifier: cancelButtonId, title: "No")
        scheduler.scheduleCategories(categories: [category])
    }
    
    func cancelAllNotifications() {
        let scheduler = WIOLocalNotification()
        scheduler.cancelAlllNotifications()
    }
    
    func cancelAllNotification(_ id: String) {
        let scheduler = WIOLocalNotification()
        scheduler.cancelNotification(id)
    }
    
    func getPendingNotificationRequests(completionHandler: @escaping ([UNNotificationRequest]) -> Void) {
        let scheduler = WIOLocalNotification()
        scheduler.getPendingNotificationRequests(completionHandler: completionHandler)
    }
    
    func getPendingNotificationRequests(id: String, completionHandler: @escaping (UNNotificationRequest?) -> Void) {
        let scheduler = WIOLocalNotification()
        scheduler.getPendingNotification(id, completionHandler: completionHandler)
    }
}
