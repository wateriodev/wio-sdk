import Foundation
import UIKit
import WioSDK

struct DoseNotification {
    let hour: Int
    let minute: Int
    let title: String
    let body: [String]
    
    var getRandomWord: String {
        return body.shuffled().first ?? title
    }
    
    func getId(day: Int) -> String {
        let notificationID = "reminder"
        let id = "\(notificationID)_\(day)_\(hour)_\(minute)"
        return id
    }
    
    static func getId(day: Int, hour: Int, minute: Int) -> String {
        let notificationID = "reminder"
        let id = "\(notificationID)_\(day)_\(hour)_\(minute)"
        return id
    }
}

final class WeeklyNotification {
    
    func delayNotificationToNextWeekByDay(reminder: DoseNotification, day: Date = Date()) {
        let day = day.getDateWithHour(hours: reminder.hour, minutes: reminder.minute)
        setupSingelNotification(reminder: reminder, date: day.nextDay(day: 7))
    }
    
    //TODO: fix later can't be duplicate notificaions
    func findDuplicateNotification(notifications: [UNNotificationRequest]) {
        var dates: [String] = []
        for notification in notifications {
            guard let date: UNCalendarNotificationTrigger = notification.trigger as? UNCalendarNotificationTrigger,
                let nextFire = date.nextTriggerDate() else {
                    continue
            }
            if dates.contains(nextFire.fullDateFormat) {
                LocalNotificaionsManager().cancelAllNotification(notification.identifier)
                fatalError("Duplicate notifications")
            }
            dates.append(nextFire.fullDateFormat)
        }
    }
    
    func fixNoifications(currntDate: Date = Date()) {
        LocalNotificaionsManager().getPendingNotificationRequests { [weak self] notifications in
            DispatchQueue.main.async {
                let filteredNotification = notifications.filter{$0.identifier.contains("reminder")}
                
                self?.findDuplicateNotification(notifications: filteredNotification)
                for not in filteredNotification where not.trigger?.repeats == false {
                    guard let date: UNCalendarNotificationTrigger = not.trigger as? UNCalendarNotificationTrigger,
                        let nextFire = date.nextTriggerDate() else {
                            return
                    }
                                        
                    if  nextFire > currntDate.nextDay(day: 7)  {
                        continue
                    }

                    let reminder = DoseNotification(hour: nextFire.hour,
                                                    minute: nextFire.minute,
                                                    title: not.content.title,
                                                    body: [not.content.body])

                    LocalNotificaionsManager().scheduleLocalNotification(id: reminder.getId(day: nextFire.dayId),
                                                                         title: reminder.title,
                                                                         body: reminder.getRandomWord,
                                                                         date: nextFire.getDateWithHour(hours: nextFire.hour, minutes: nextFire.minute),
                                                                         repeats: .weekly)
                }
                self?.printAll()
            }
        }
    }
    
    func setupNotification(reminders: [DoseNotification]) {
        for reminder in reminders {
            for index in 0...6 {
                let day = Date().getDateWithHour(hours: reminder.hour,
                                                 minutes: reminder.minute).nextDay(day: index)
                setupNotification(reminder: reminder, date: day)
            }
        }
        printAll()
    }
    
    private func setupNotification(reminder: DoseNotification, date: Date) {
        LocalNotificaionsManager().scheduleLocalNotification(id: reminder.getId(day: date.dayId),
                                                             title: reminder.title,
                                                             body: reminder.getRandomWord,
                                                             date: date,
                                                             repeats: .weekly)
    }
    
    private func setupSingelNotification(reminder: DoseNotification, date: Date) {
        LocalNotificaionsManager().scheduleLocalNotification(id: reminder.getId(day: date.dayId),
                                                             title: reminder.title,
                                                             body: reminder.getRandomWord,
                                                             date: date,
                                                             repeats: .none)
        printAll()
    }
}


extension Date {
    
    var hour: Int {
        let newComponents = Calendar.current.dateComponents([.hour, .minute], from: self)
        return newComponents.hour ?? 0
    }
    
    var minute: Int {
        let newComponents = Calendar.current.dateComponents([.hour, .minute], from: self)
        return newComponents.minute ?? 0
    }
    
    var dayId: Int {
        let newComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: self)
        let weekday = newComponents.weekday ?? 0
        return weekday
    }
    
    var day: Int {
        let newComponents = Calendar.current.dateComponents([.weekday, .hour, .minute], from: self)
        let weekday = newComponents.weekday ?? 0
        return weekday
    }
    
    func getDateWithHour(hours: Int, minutes: Int) -> Date {
        let date = Calendar.current.date(bySettingHour: hours,
                                         minute: minutes,
                                         second: 0, of: self)!
        return date
    }
    
    func adding(minutes: Int) -> Date {
        return Calendar.current.date(byAdding: .minute, value: minutes, to: self)!
    }
    
    func nextDay(day: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: day, to: self)!
    }
    
    func nextDay() -> Date {
        return Calendar.current.date(byAdding: .day, value: 1, to: self)!
    }
    
    func nextWeek() -> Date {
        return Calendar.current.date(byAdding: .weekOfYear, value: 1, to: self)!
    }
}

extension WeeklyNotification {
    func printAll() {
//        #if DEBUG
//        LocalNotificaionsManager().getPendingNotificationRequests { (notifications) in
//            DDLogInfo(notifications.map{
//                let trigger: UNCalendarNotificationTrigger = $0.trigger as! UNCalendarNotificationTrigger
//                
//                DDLogInfo("===\($0.identifier)==Repeats=\(trigger.repeats)" + trigger.nextTriggerDate()!.day )
//            })
//            DDLogInfo("---------COUNT\(notifications.count)------------")
//        }
//        #endif
    }
}

