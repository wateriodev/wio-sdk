//
//  WioNetwork.swift
//  WioSDK_Example
//
//  Created by israel on 27/04/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK

final class WIONetwork {
    var wioAPI:  WIOApiType {
        return WIOApp.shared.networkManager.wioApi
    }
    
    var wioAnalytics: WIOAnalytics {
        return WIOApp.shared.networkManager.wioAnalytics
    }
    
    func start() {
        if wioAPI.userId ?? 0 > 0 {
            return
        }
        print("User-id: \(String(describing: wioAPI.userId))")
        wioAPI.register(email: Configuration.email,
                     password: Configuration.password) {  restut in

        }
    }
}

//MARK: Analytics extension WIONetwork {
extension WIONetwork {
    func report() {
        wioAnalytics.startApp()
    }
}
