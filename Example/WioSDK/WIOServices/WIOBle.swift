//
//  WioBle.swift
//  WioSDK_Example
//
//  Created by israel on 27/04/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK
import WaterIOBleKit

final class WIOBle {
    private var bleManager: WIOBleManager {
        return WIOApp.shared.bleMenegaer
    }
    
    func start() {
        bleManager.capCommandSender.getMacAddress { (macaddress) in }
        sendCommand()
    }
    
    func startListen() {
       WIOApp.shared.bleMenegaer.processor.delegate = self
    }
}

//MARK: Connect to cap
extension WIOBle {
    func startScan() {
        bleManager.capConnection.startScan()
    }
    
    func stopScan() {
        bleManager.capConnection.stopScan()
    }
    
    func forgetCap() {
        bleManager.capConnection.forgetCap()
    }
}

//MARK: Send command
extension WIOBle {
    func sendCommand() {
        //bleManager.capCommandSender.runBlinking()
    }
}

//MARK: listen to envets from cap
extension WIOBle: WIOCapDataProcessorDelegate {
    func capDataModelDidUpdateEvents(events: [CapEvent]) {
        
    }
    
    func didUpdateCapInfo(_ capVersion: String) {
        
    }
}
