//
//  HomeCellSections.swift
//  WioSDK_Example
//
//  Created by israel water-io on 01/10/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation

struct HomeSection {
    var rows: [CellConfigurator]
    
    mutating func appendCell(_ data: CellConfigurator) {
        self.rows.insert(data, at: 0)
    }
}
