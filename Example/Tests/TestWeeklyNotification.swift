//
//  TestWeeklyNotification.swift
//  WioSDK_Example
//
//  Created by israel water-io on 23/08/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation

final class TestWeeklyNotification {
    
    let weeklyNotificaiton: WeeklyNotification
    var notificatoins1 = [
        DoseNotification(hour: 10, minute: 50, title: "Hey", body: ["Body"]),
        DoseNotification(hour: 14, minute: 12, title: "Hey", body: ["Body"])
    ]
    
    var notificatoins2 = [
        DoseNotification(hour: 15, minute: 50, title: "Hey", body: ["Body"]),
        DoseNotification(hour: 12, minute: 12, title: "Hey", body: ["Body"])
    ]
    
    init() {
        weeklyNotificaiton = WeeklyNotification()
        weeklyNotificaiton.fixNoifications(currntDate: Date())
    }
    
    func startTest() {
        setupNotificaiton()
       
        DispatchQueue.main.asyncAfter(deadline: .now() + 5) {
            self.pillTaken()
        }
    }
    
    func setupNotificaiton() {
        weeklyNotificaiton.setupNotification(reminders: notificatoins1)
    }
    
    func pillTaken() {
        weeklyNotificaiton.delayNotificationToNextWeekByDay(reminder: notificatoins1[0], day: Date())
    }
    
}
