//
//  NetworkTest.swift
//  WioSDK_Example
//
//  Created by israel on 30/06/2020.
//  Copyright © 2020 CocoaPods. All rights reserved.
//

import Foundation
import WioSDK

final class NetworkTest {
    
    var wioAPI:  WIOApiType {
        return WIOApp.shared.networkManager.wioApi
    }
    
    var wioAnalytics:  WIOAnalytics {
        return WIOApp.shared.networkManager.wioAnalytics
    }
    
    func start() {
        //register()
        
        if wioAPI.userId == nil {
            register()
        } else {
            next()
        }
    }
    
    var count = 9
    
    func next() {
        switch count {
        case 9:
            getQuotes()
        case 1:
            getCalendarDailyGoal()
        case 2:
            getDailyHydration()
        case 3:
            setNotificationReminder()
        case 4:
            setTimeRangeHydrationDay()
        case 5:
            setManualDailyGoal()
        default:
            print("Testing is finish")
            return
            
        }
        count += 1
    }
    
    func error() {
        print("ERRORRRRR")
    }
}

//MARK: Analytics extension WIONetwork {
extension NetworkTest {
    func report() {
        wioAnalytics.startApp()
    }
}

//MARK: Login & Profile
extension NetworkTest {

    func getVitaminsList() {
        wioAPI.getVitaminsList { result in
            switch result {
            case .success(let response):
                print("getVitaminsList response:" + "\(response)")
                self.next()
            case .error(let error):
                self.error()
                print("getVitaminsList error:" + "\(error)")
            }
        }
    }
    
    func getUserScore() {
        wioAPI.getUserScore { result in
            switch result {
            case .success(let response):
                print("getUserScore response:" + "\(response)")
                self.next()
            case .error(let error):
                self.error()
                print("getUserScore error:" + "\(error)")
            }
        }
    }
    
    func getTermsAndConditionsURL() {
        wioAPI.getTermsAndConditionsURL { result in
            switch result {
            case .success(let response):
                self.next()
                print("getTermsAndConditionsURL response:" + "\(response)")
            case .error(let error):
                self.error()
                print("getTermsAndConditionsURL error:" + "\(error)")
            }
        }
    }
    
    func getSystemConfigurations() {
        wioAPI.getSystemConfigurations { result in
            switch result {
            case .success(let response):
                self.next()
                print("getSystemConfigurations response:" + "\(response)")
            case .error(let error):
                self.error()
                print("getSystemConfigurations error:" + "\(error)")
            }
        }
    }
    
    func getPrivacyURL() {
        wioAPI.getPrivacyURL { result in
            switch result {
            case .success(let response):
                self.next()
                print("getPrivacyURL response:" + "\(response)")
            case .error(let error):
                self.error()
                print("getPrivacyURL error:" + "\(error)")
            }
        }
    }
    
    
    func login() {
        wioAPI.login(email: Configuration.email,
                     password: Configuration.password) { result in
                        switch result {
                        case .success(let response):
                            self.next()
                            print("login response:" + "\(response)")
                        case .error(let error):
                            self.error()
                            print("login error:" + "\(error)")
                        }
        }
    }
    
    func register() {
        wioAPI.register(email: Configuration.email,
                        password: Configuration.password) { result in
                            switch result {
                            case .success(let response):
                                self.next()
                                print("register success:" + "\(response)")
                            case .error(let error):
                                self.error()
                                print("register error:" + "\(error)")
                            }
        }
    }
    
    func updateProfile() {
        let json = [
            "email":"israel@gmail.com",
            "nick_name":"israel",
            "first_name":"persiko",
            "age": 28,
            "weight": 73,
            "height": 180,
            "gender": "M",
            "mobile_phone": "0509875132",
        ]
            as [String : Any]
       
        wioAPI.updateProfile(json: json) { result in
            switch result {
            case .success(let response):
                self.next()
                print("updateProfile success:" + "\(response)")
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
}



extension NetworkTest {

    func getQuotes() {
        wioAPI.getQuotes { result in
            switch result {
            case .success(let response):
                print("updateProfile success:" + "\(response)")
                self.next()
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
    
    func getCalendarDailyGoal() {
        wioAPI.getCalendarDailyGoal(date: "2020-05", timeOffset: "-3") { result in
            switch result {
            case .success(let response):
                print("updateProfile success:" + "\(response)")
                self.next()
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
    
    func getDailyHydration() {
        wioAPI.getDailyHydration(date: "2020-05-01", timeOffset: "-3") { result in
            switch result {
            case .success(let response):
                self.next()
                print("updateProfile success:" + "\(response)")
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
    
    func setManualDailyGoal() {
        wioAPI.setManualDailyGoal(isManual: true, goal: 2000) { result in
            switch result {
            case .success(let response):
                self.next()
                print("updateProfile success:" + "\(response)")
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
    
    func setNotificationReminder() {
        wioAPI.setNotificationReminder(type: "CAP", interval: "30_MIN") { result in
            switch result {
            case .success(let response):
                self.next()
                print("updateProfile success:" + "\(response)")
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
    
    func setTimeRangeHydrationDay() {
        wioAPI.setTimeRangeHydrationDay(startDay: "09:10", endDay: "21:00") { result in
            switch result {
            case .success(let response):
                self.next()
                print("updateProfile success:" + "\(response)")
            case .error(let error):
                self.error()
                print("updateProfile error:" + "\(error)")
            }
        }
    }
}
