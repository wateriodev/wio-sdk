//
//  WIOCapInfo.swift
//  WioSDK
//
//  Created by israel water-io on 29/09/2020.
//

import Foundation

public struct WIOCapInfo {
    public let uuid: String
    public let version: String?
    public let macAddress: String?
    
    init(UUID: String, version: String? = nil, macAddress: String? = nil) {
        self.uuid = UUID
        self.version = version
        self.macAddress = macAddress
    }
}
