import Foundation
import WaterIOBleKit

public struct CapEvent {
    public let timestamp: UInt32
    public let value: UInt16
    public var opCode: EventCode
    public let stdDeviation: UInt8
    public let isFromLogs: Bool
    public var eventData: Data?
    
    init(entry: CapEventEntry) {
        self.timestamp = entry.timestamp
        self.opCode = EventCode(rawValue: entry.code) ?? .unknown
        self.value = entry.value
        self.stdDeviation = entry.stdDeviation
        self.isFromLogs = entry.isFromLogs
        self.eventData = entry.eventData
    }
}
