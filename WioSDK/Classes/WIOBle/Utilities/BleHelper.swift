//
//  BleHelper.swift
//  WioSDK
//
//  Created by israel on 13/05/2020.
//

import Foundation

class BleHelper {
    static func isCapNotSupportMultipleReminders(capVersion: String) -> Bool {
        let notSupportVersions: [String] = [
            "01.08.0046V10",
            "01.08.0046V09",
            "01.08.0046V08",
            "01.08.0046V07"]
        return notSupportVersions.contains(capVersion)
    }
}
