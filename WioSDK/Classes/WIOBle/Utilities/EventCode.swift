//
//  EventCode.swift
//  WioSDK
//
//  Created by israel on 21/11/2019.
//

import Foundation

public enum EventCode: UInt8, RawRepresentable {
    case pair               = 0x50
    case open               = 0x4f
    case close              = 0x43
    case appConnected       = 0x41
    case appDisconnected    = 0x44
    case unknown            = 0x00
    case length             = 0x4C
    case measure            = 0x4D
    case reset              = 0x90

    case preRemind          = 0x72
    case remind             = 0x52
    case amount             = 0x57
}
