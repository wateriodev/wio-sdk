import Foundation
import WaterIOBleKit
import WaterIONetwork

public protocol WIOCapDataProcessorDelegate: class {
    //func capDataModelDidUpdateEvents(_ capDataModel: CapDataModel, eventsList: CapEventsList, isCapOpen: Bool?)
    func capDataModelDidUpdateEvents(events: [CapEvent])
    func didUpdateCapInfo(_ capVersion: String)
}

public class WIOCapDataProcessor {
    
    public weak var delegate: WIOCapDataProcessorDelegate?
    private let bleManager: BleManagerType
    
    public init(bleManager: BleManagerType) {
        self.bleManager = bleManager
    }
    
    func start() {
        self.bleManager.bleManagerDelegate = self
    }
}

extension WIOCapDataProcessor: BleManagerDelegate {
    public func bleManager(_ bleManager: BleManagerType, restoredDevice: [BleDevice]?) {
        for currntCap in restoredDevice ?? [] {
            currntCap.delegate = self
        }
    }
    
    public func bleManager(_ bleManager: BleManagerType, didConnectedToNewDevice device: BleDevice) {
        device.delegate = self
    }
    
    public func bleManager(_ bleManager: BleManagerType, didUpdateConnection isConnect: Bool, cap: BleDevice?) {
        
    }
}
extension WIOCapDataProcessor: BleDeviceDelegate {
    public func capDataModelDidUpdateEvents(_ device: BleDevice,
                                            capDataModel: CapDataModel,
                                            eventsList: CapEventsList,
                                            isCapOpen: Bool?,
                                            capId: String) {
        let map = eventsList.events.map{ CapEvent(entry: $0) }
        delegate?.capDataModelDidUpdateEvents(events: map)
        reportEvent(events: eventsList.events)
    }
    
    public func bleDevice(_ device: BleDevice, onFinishConfiguration isFirstConfiguration: Bool) {
        
    }
    
    public func didUpdateCapInfo(_ capInfo: CapInfo) {
        delegate?.didUpdateCapInfo(capInfo.capVersion)
    }
}

