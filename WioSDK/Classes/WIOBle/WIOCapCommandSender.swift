import Foundation
import WaterIOBleKit
import CocoaLumberjack

public final class WIOCapCommandSender {
    
    private let bleManager: BleManagerType
    private let capCommandSender: CommandSenderType
    
    public init(bleManager: BleManagerType) {
        self.bleManager = bleManager
        self.capCommandSender = CommandSender(bleMnagaer: bleManager as! BleManager)
    }
    
    func start() {
        
    }
}

extension WIOCapCommandSender: CommandSenderType {
    
    public func sendCustomCommand(command: CapCommandAPI, saveLater: Bool) {
        capCommandSender.sendCustomCommand(command: command, saveLater: saveLater)
    }
    
    public func getMacAddress(onCompelte: @escaping (String?) -> Void) {
        capCommandSender.getMacAddress {  _ in
            onCompelte(CurrentDevice.currentDevice()?.MACAddress)
        }
    }
    
    public func setBlinkingTime(duration: Int) {
        capCommandSender.setBlinkingTime(duration: duration)
    }
    
    public func setReminders(times: [(hours: Int, minutes: Int)]) {
        let capVersion = CapInfo.currentCap()?.capVersion ?? ""
        
        if BleHelper.isCapNotSupportMultipleReminders(capVersion: capVersion) {
            capCommandSender.setReminders(times: [times.first!])
        } else {
            capCommandSender.setReminders(times: times)
        }
        checkCapConnection()
    }
    
    public func dailyUsage(from: (hours: Int, minutes: Int),
                           to: (hours: Int, minutes: Int)) {
        capCommandSender.dailyUsage(from: from, to: to)
        checkCapConnection()
    }
    
    public func runBlinking() {
        capCommandSender.runBlinking()
        checkCapConnection()
    }
    
    public func playSound() {
        capCommandSender.playSound()
        checkCapConnection()
    }
}

extension WIOCapCommandSender {
    
    func checkCapConnection() {
        if bleManager.currntCap == nil {
            DDLogInfo("ERROR: Please connect cap first you command remove when cap will connect")
        } else {
            DDLogInfo("awating to cap connection")
        }
    }
}
