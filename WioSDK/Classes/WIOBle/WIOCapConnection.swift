import Foundation
import WaterIOBleKit
import CoreBluetooth
import CocoaLumberjack

struct CapConnectionConstance {
    static let onDeviceConnected: String = "ble.cap.connected"
}

public class WIOCapConnection: WIOCapConnectionType {
    private var rssiFilter: Int?
    
    private var isYourCap: ((Bool) -> Void)?
   
    public var waitToCheckIsMyCap: Bool? {
        didSet {
            // true: isYour cap
            isYourCap?(waitToCheckIsMyCap ?? false)
        }
    }
    
    public var bleState: WIOBleState {
        switch bleManager.managerState {
        case .unknown:
            return WIOBleState.unknown
        case .resetting:
            return WIOBleState.resetting
        case .unsupported:
            return WIOBleState.unsupported
        case .unauthorized:
            return WIOBleState.unauthorized
        case .poweredOff:
            return WIOBleState.poweredOff
        case .poweredOn:
            return WIOBleState.poweredOn
        @unknown default:
            fatalError()
        }
    }
    
    public weak var delegate: WIOCapConnectionDelegate?
    
    public var currentCapUUID: String? {
        return CurrentDevice.currentDevice()?.UUID
    }
    
    public var state: WIOConnectionState {
        didSet {
            self.delegate?.didDeviceConnectionStateChange(state: state)
        }
    }
    public var statusText: String = ""
    public var capUUID: String = ""
    
    //Private
    private let bleManager: BleManagerType
    private var isStartingToScan: Bool = false
   
    private var isDeviceConnected: Bool {
        return bleManager.isDeviceConnected
    }
    
    private var isDeviceRegisterd: Bool {
        return CurrentDevice.currentDevice() != nil
    }
    
    private let version: CapVersionType
   
    deinit {
        if bleManager.isScanning {
            bleManager.stopScan()
        }
        NotificationCenter.default.removeObserver(self)
    }
    
    public init(bleManager: BleManagerType, version: CapVersionType) {
        self.bleManager = bleManager
        self.version = version
        state = .unknown
    }
    
    internal func start() {
        setupNotification()
        self.bleManager.scanDelegate = self
        listenToCapChanged()
        updateState()
    }
    
    public func setCapVersion(_ version: CapVersionType) {
        bleManager.capVersion = version
    }
      
    public func startScan(rssiFilter: Int? = nil) {
        self.rssiFilter = rssiFilter
        if isDeviceRegisterd {
            forgetCap()
        }
        scanForDevices()
    }
    
    
    public func stopScan() {
        bleManager.stopScan()
    }
    
    public func forgetCap(completion: (()->())? = nil) {
        isStartingToScan = false
        bleManager.forgetDevice(nil, completion: completion)
        self.onDidDisconnectedToDevice()
        updateState()
    }
    
    private func onNewDeviceConnect() {
       
        var capId: String? {
            if let macAddress = CurrentDevice.currentDevice()?.MACAddress {
                return macAddress
            }
            if let UUID = CurrentDevice.currentDevice()?.UUID {
                return UUID
            }
            return nil
        }
        
        NotificationCenter.default.post(name: NSNotification.Name(CapConnectionConstance.onDeviceConnected),
                                        object: capId)
        isStartingToScan = false
    }
}

extension WIOCapConnection: BleManagerScanDelegate {
    public func bleManager(didDiscover bleManager: BleManagerType, name: String, uuid: String) {
        self.delegate?.didCapFound(uuid, name: name)
    }

    public func filter(_ bleManager: BleManagerType, RSSI: Int, UUID: String) -> Bool? {
        guard let filterRssi = self.rssiFilter else {
            return true
        }
        
        if RSSI < filterRssi {
            return false
        }
       
        return true
    }
    
    public func bleManager(isWatingToCheckIsMyCap bleManager: BleManagerType) -> Bool? {
        return waitToCheckIsMyCap
    }
    
    public func bleManager(_ bleDevice: BleDevice,
                           isYourCap complition: @escaping (Bool) -> Void) {
        if waitToCheckIsMyCap == false || waitToCheckIsMyCap == nil {
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.2) {
                complition(true)
            }
            return
        }
        
        isYourCap = { isYourCap in complition(!isYourCap) }
    }
    
    public func bleManager(_ bleManager: BleManagerType, centralStateDidChange state: CBManagerState) {
        if state != .poweredOn {
            if case .scanning = self.state {
                bleManager.stopScan()
            }
        } else if isStartingToScan && CurrentDevice.currentDevice() == nil {
            scanForDevices()
        }
        updateState()
    }
    
    public func bleManager(_ bleManager: BleManagerType, didUpdateConnection isConnect: Bool, cap: BleDevice?) {
        self.delegate?.didUpdateConnection(isConnect)
        updateState()
    }
    
    public func bleManager(_ bleManager: BleManagerType, didFailToConnect UUID: String) {
        
    }
}


private extension WIOCapConnection {
    
    func setupNotification() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(applicationDidBecomeActive(_:)),
                                               name: UIApplication.didBecomeActiveNotification, object: nil)
    }
    
    @objc func applicationDidBecomeActive(_ notification: NSNotification) {
        if CurrentDevice.currentDevice() == nil && isStartingToScan {
            scanForDevices()
        }
    }
}

private extension WIOCapConnection {
    func updateState() {
        let oldState: WIOConnectionState = state
        let newState: WIOConnectionState
        let isScanning = bleManager.isScanning
        let managerState = bleManager.managerState
        
        if let device = CurrentDevice.currentDevice() {
            capUUID = device.UUID
            if case .scanning = oldState {
                newState = .connected(newDevice: true)
                onNewDeviceConnect()
            } else {
                newState = .connected(newDevice: false)
            }
            statusText = "Connected."
        } else if isScanning && managerState == .poweredOn {
            capUUID = ""
            newState = .scanning
            statusText = "Open or Close the cap to connect"
        } else if managerState != .poweredOn {
            switch managerState {
            case .resetting:
                statusText = "Bluetooth is restting"
                newState = .bluetoothIssue
            case .unauthorized:
                statusText = "Bluetooth unautorized"
                newState = .unauthorized
            case .unsupported:
                statusText = "Bluetooth is unsupported"
                newState = .bluetoothIssue
            case .poweredOff:
                statusText = "Bluetooth is off. Use the control center to turn it on"
                newState = .bluetoothIssue
            default:
                statusText = "Unknown issue"
                newState = .bluetoothIssue
            }
        } else {
            capUUID = ""
            newState = .unknown
            statusText = ""
        }
        
        DDLogInfo("WIOCapConnection \(newState)")
        self.state = newState
    }
    
    func scanForDevices() {
        DDLogInfo("WIOCapConnection scanForDevices \(bleManager.managerState)")

        if bleManager.managerState != .poweredOn {
            updateState()
            return
        }
       
        if bleManager.isScanning {
            return
        }
        
        //bleManager.onDidConnectToDevice = { [weak self] in self?.onDidConnectToDevice() }
        bleManager.startScan(nil)
        isStartingToScan = true
        updateState()
    }
    
    func onDidDisconnectedToDevice() {
        DDLogInfo("WIOCapConnection onDidDisconnectedToDevice")
        updateState()
    }
    
    func onDidConnectToDevice() {
        isStartingToScan = false
        updateState()
    }
}

extension WIOCapConnection {
   
    private func listenToCapChanged() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(onDeviceInfoDidChanged(notification:)),
                                               name: NSNotification.Name(rawValue: CurrentDevice.deviceDidChange),
                                               object: nil)
    }
    
    @objc func onDeviceInfoDidChanged(notification: NSNotification) {
        guard let UUID = CurrentDevice.currentDevice()?.UUID else {
            return
        }
        
        let MACAddress = CurrentDevice.currentDevice()?.MACAddress
        let version = CapInfo.currentCap()?.capVersion
        let capInfo = WIOCapInfo(UUID: UUID, version: version, macAddress: MACAddress)
        delegate?.didCapInfoUpdated(capInfo)
    }
}
