import Foundation
import WaterIOBleKit

public enum WIOConnectionState {
    case unknown
    case connected(newDevice: Bool)
    case scanning
    case bluetoothIssue
    case unauthorized
}

public enum WIOBleState {
    case unknown
    case scanning
    case unauthorized
    case resetting
    case unsupported
    case poweredOff
    case poweredOn
}

public protocol WIOCapConnectionType {
    
    var statusText: String { get }
    var state: WIOConnectionState { get }
    var bleState: WIOBleState { get }

    var delegate: WIOCapConnectionDelegate? { get set }

    func startScan(rssiFilter: Int?)
    func stopScan()
    func forgetCap(completion: (()->())?)
    
    func setCapVersion(_ version: CapVersionType)
}

public protocol WIOCapConnectionDelegate: class {
    func didUpdateConnection(_ isConnected: Bool)
    func didDeviceConnectionStateChange(state: WIOConnectionState)
    func didCapInfoUpdated(_ capInfo: WIOCapInfo)
    func didCapFound(_ uuid: String, name: String)
}

public extension WIOCapConnectionDelegate {
    func didCapInfoUpdated(_ capInfo: WIOCapInfo) {}
    func didCapFound(_ uuid: String, name: String) {}
}

