//
//  SendLogsHelper.swift
//  WaterIO
//
//  Created by Nissan Tsafrir.
//

import Foundation
import MessageUI
import CocoaLumberjack

final class LogFileInfo {
    let fileName: String
    let data: Data
    let path: URL
    
    init(fileName: String, data: Data, path: URL) {
        self.fileName = fileName
        self.data = data
        self.path = path
    }
}


class SendLogsController: NSObject {

    static var fileLogger: DDFileLogger?
    
    var mailComposeDelegate: MFMailComposeViewControllerDelegate?
    
    var presentingViewController: UIViewController? = UIApplication.shared.keyWindow?.rootViewController
    
  
    open func sendLogs() {
    
        guard let fileLogger = SendLogsController.fileLogger else {
            print("Missing Logger")
            return
        }
        
        fileLogger.rollLogFile { 
            DispatchQueue.main.async {
                self.didRollLogFiles()
            }
        }
    }
    
    fileprivate func didRollLogFiles() {
        guard let fileLogger = SendLogsController.fileLogger else {
            print("Missing Logger")
            return
        }
        let fileManager: DDLogFileManager = fileLogger.logFileManager
        
        var logFiles: [LogFileInfo] = []
        
        let files: [DDLogFileInfo] = fileManager.sortedLogFileInfos
            for fileInfo in files {
                let url = URL(fileURLWithPath: fileInfo.filePath)
                guard let data = try? Data(contentsOf: url) else {
                        print("can't read log file")
                        continue
                }
                logFiles.append(LogFileInfo(fileName: fileInfo.fileName, data: data, path: url))
            }
    
        
        if logFiles.count > 0 {
            sendLogsWithFiles(Array(logFiles.prefix(2)))
        } else {
            print("Error: no valid log files to send")
        }
    }
    
    fileprivate func sendLogsWithFiles(_ logFiles: [LogFileInfo]) {
        guard MFMailComposeViewController.canSendMail() else {
            return
        }
        
        let vc = MFMailComposeViewController()
        vc.mailComposeDelegate = self
        
        // swiftlint:disable force_unwrapping
        let name = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        let bundleVersion = Bundle.main.infoDictionary![kCFBundleVersionKey as String] as! String
        let subject = "[Report Problem] Logs for '\(name)' (\(bundleVersion))"
        vc.setSubject(subject)
        //vc.setToRecipients(["YOUREMAIL"])
        
        guard let rootVC = presentingViewController else { return }
        
        mailComposeDelegate = self
        vc.mailComposeDelegate = mailComposeDelegate
        
        vc.setMessageBody(appInfo(), isHTML: false)
        
        for file in logFiles {
            vc.addAttachmentData(file.data, mimeType: "text/plain", fileName: file.fileName)
        }
        rootVC.present(vc, animated: true, completion: nil)
    }
    
    fileprivate func appInfo() -> String {
        let pi = ProcessInfo.processInfo
        
        let name = Bundle.main.infoDictionary![kCFBundleNameKey as String] as! String
        
        let info = "\n---\nname:\(name)\nversion:\(version())\nos: \(pi.operatingSystemVersionString)"
        
        DDLogInfo("arguments: \(pi.arguments)")
        
        return info
    }
    
    fileprivate func version() -> String {
        let shortVersion = Bundle.main.infoDictionary!["CFBundleShortVersionString"] as! String
        let bundleVersion = Bundle.main.infoDictionary![kCFBundleVersionKey as String] as! String //CFBundleGetVersionNumber
        return "\(shortVersion) (\(bundleVersion))"
    }
}

extension SendLogsController: MFMailComposeViewControllerDelegate {
    
    public func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        print("mail reuslt: \(result.rawValue)")
        if result == MFMailComposeResult.sent {
            print("email sent")
        }
        
        controller.dismiss(animated: true, completion: nil)
        mailComposeDelegate = nil
    }
}





class LogHelper {
    
    open func getLogsFiles(onFinished: @escaping ([LogFileInfo]?) -> Void) {
        guard let fileLogger = SendLogsController.fileLogger else {
            print("Missing Logger")
            return
        }
        
        fileLogger.rollLogFile {
            DispatchQueue.main.async {
                onFinished(self.didRollLogFiles())
            }
        }
         onFinished(nil)
    }
    
    fileprivate func didRollLogFiles() ->  [LogFileInfo]? {
        guard let fileLogger = SendLogsController.fileLogger else {
            print("Missing Logger")
            return nil
        }
        let fileManager: DDLogFileManager = fileLogger.logFileManager
        
        var logFiles: [LogFileInfo] = []
        
        let files: [DDLogFileInfo] = fileManager.sortedLogFileInfos
            for fileInfo in files {
                let url = URL(fileURLWithPath: fileInfo.filePath)
                guard let data = try? Data(contentsOf: url) else {
                        print("can't read log file")
                        continue
                }
                logFiles.append(LogFileInfo(fileName: fileInfo.fileName, data: data, path: url))
            }
    
        
        if logFiles.count > 0 {
            //sendLogsWithFiles(Array(logFiles.prefix(2)))
            return Array(logFiles.prefix(2))
        } else {
            print("Error: no valid log files to send")
        }
        
        return nil
    }
}
