//
//  LogSender.swift
//  WioSDK
//
//  Created by israel water-io on 27/07/2020.
//

import Foundation
import CocoaLumberjack
import MessageUI

final class LogSender {
    
    func setupLogger(isDebugMode: Bool = false) {
    
        if isDebugMode {
            dynamicLogLevel = DDLogLevel.verbose
        } else {
            dynamicLogLevel = DDLogLevel.verbose
            //dynamicLogLevel = DDLogLevel.error
        }
        
        DDLog.add(DDOSLogger.sharedInstance)
        let fileLogger = DDFileLogger(logFileManager: DDLogFileManagerDefault())
        fileLogger.rollingFrequency = 60 * 60 * 48 // 48 hours
        DDLog.add(fileLogger)
        SendLogsController.fileLogger = fileLogger
    }
    
    func showSendLogs() {
        let vc = ServiceViewController.createSendLogsViewController(true)
        let root = UIApplication.shared.keyWindow?.rootViewController
        root?.present(vc, animated: true, completion: nil)
    }
}
