//
//  ServiceViewController.swift
//  WaterIO
//
//  Created by Nissan Tsafrir.
//

import UIKit

class ServiceViewController: UIViewController {
    
    lazy var spinner: UIActivityIndicatorView = {
        let spinner = UIActivityIndicatorView()
        spinner.center = self.view.center
        spinner.color = .black
        return spinner
    }()
        
    var addDoneButton: Bool!
    
    var didShowSendLogs = false
    
    func setupView() {
        self.view.backgroundColor = .white
        self.view.addSubview(spinner)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupView()
        self.title = "Send Logs"
        
        if let addDoneButton = addDoneButton, addDoneButton == true {
    self.navigationItem.leftBarButtonItem = UIBarButtonItem(barButtonSystemItem: .done,
                                                            target: self,
                                                            action: #selector(doneButtonPressed))
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if !didShowSendLogs {
            let helper = SendLogsController()
            helper.presentingViewController = self
            helper.sendLogs()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if !didShowSendLogs {
            didShowSendLogs = true
            spinner.isHidden = true
        }
    }
    
    @objc func doneButtonPressed() {

        UIApplication.shared.keyWindow?.rootViewController?.dismiss(animated: true,
                                                                    completion: nil)
    }
    
    class func createSendLogsViewController(_ addDoneButton: Bool) -> UINavigationController {
        let vc = ServiceViewController(nibName: nil, bundle: nil)
        vc.addDoneButton = addDoneButton
        let nvc = UINavigationController(rootViewController: vc)
        return nvc
    }
}
