//
//  WIONotificationsManager.swift
//  WioSDK
//
//  Created by israel on 21/05/2020.
//

import Foundation
import CocoaLumberjack

class WIOConstance {
    static let onPushNotificaitonRecived = "push.onPushNotificaitonRecived"
}

public protocol WIOPushNotificationManagerDelegate: class {
    func onUserClickAction(_ notificationId: String, actionIdentifier: String)
    func onPushReceived(userInfo: [AnyHashable : Any]?)
}

public protocol WIONotificationsManagerType {
    func onDidRegisterForRemoteNotifications(withDeviceToken token: Data?)
    func onDidFailToRegisterForRemoteNotificationsWithError(_ error: Error?)
    func onUserClickAction(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse)
    func onPushReceived(userInfo: [AnyHashable : Any]?, onStart: Bool)
    func routePush(category: String, userInfo: [AnyHashable : Any]?)
}

public class WIONotificationsManager: NSObject {
    
    public weak var delegate: WIOPushNotificationManagerDelegate?
    
    private let configuration: PushConfiguration
    private let networkManager: WIONetworkManager
    private let firebasePushService: FirebasePushService
    private let isSendbox: Bool

    public init(networkManager: WIONetworkManager, configuration: PushConfiguration, isSendbox: Bool) {
        self.isSendbox = isSendbox
        self.networkManager = networkManager
        self.configuration = configuration
        self.firebasePushService = FirebasePushService(isSendbox: isSendbox)
        super.init()
        self.firebasePushService.userId = networkManager.wioApi.userId
    }
    
    public func start(onCompelte: ((Bool) -> Void)? = nil) {
        WIONotificationHelper.askPermission { [weak self] isAccept in
            DDLogInfo("Notification isAccept:\(isAccept)")
            if isAccept  {
                self?.configure()
            }
            
            if let onCompelte = onCompelte {
                onCompelte(isAccept)
            }
        }
    }
    
    func configure() {
        if networkManager.wioApi.userId == nil {
            DDLogInfo("ERROR: user-id missing")
            return
        }
        UIApplication.shared.registerForRemoteNotifications()
        firebasePushService.startFirebase(option: configuration)
        WIOPushCategory().setupAllCategories()
    }
}

extension WIONotificationsManager: WIONotificationsManagerType {

    public func onDidRegisterForRemoteNotifications(withDeviceToken token: Data?) {
        guard let token = token else {
            fatalError("Invalid Device Token")
        }
        firebasePushService.onDidRegisterForRemoteNotifications(deviceToken: token)
    }

    public func onDidFailToRegisterForRemoteNotificationsWithError(_ error: Error?) {
        DDLogInfo("Invalid Device Token \(String(describing: error))")
    }

    public func onUserClickAction(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse) {
        let userInfo = response.notification.request.content.userInfo
        let notificationId = response.notification.request.content.categoryIdentifier
        routePush(category: notificationId, userInfo: userInfo)
        firebasePushService.onPushReceived(userInfo: userInfo, onStart: false)
        delegate?.onUserClickAction(notificationId, actionIdentifier: response.actionIdentifier)
        self.delegate?.onPushReceived(userInfo: userInfo)
        DDLogInfo("user click Action \(response.actionIdentifier)")
    }

    public func onPushReceived(userInfo: [AnyHashable : Any]?, onStart: Bool) {
        firebasePushService.onPushReceived(userInfo: userInfo, onStart: onStart)
        self.delegate?.onPushReceived(userInfo: userInfo)
        if let dics = userInfo as? [String: Any],
            let apn = dics["aps"] as? [String: Any],
            let category = apn["category"] as? String {
            routePush(category: category, userInfo: userInfo)
        }
    }

    public func routePush(category: String, userInfo: [AnyHashable : Any]?) {
        let state = WIOPushCategoryType(rawValue: category) ?? .unknow
        switch state {
        case .unknow:
            return
        case .newChatMessage:
            let notificationName = WIOConstance.onPushNotificaitonRecived
            NotificationCenter.default.post(name: Notification.Name(notificationName), object: userInfo)
        case .pushWithButton:
            let notificationName = WIOConstance.onPushNotificaitonRecived
            NotificationCenter.default.post(name: Notification.Name(notificationName), object: userInfo)
        }
    }
}


