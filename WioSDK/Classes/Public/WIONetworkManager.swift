//
//  WIONetworkManager.swift
//  WioSDK
//
//  Created by israel on 30/04/2020.
//

import Foundation
import WaterIONetwork
import WaterIOBleKit
import CocoaLumberjack

public class WIONetworkManager {
    
    public var wioApi: WIOApiType
    public let wioAnalytics: WIOAnalytics
    
    internal let analytics: FirebaseClient
    internal let waterIOClient: WaterIOClient
    internal let sdkMessaging: SDKMessagingReport
    internal var appConfig: AppConfigService!

    internal let appToken: String
    internal let appType: String
    private var isUpdateToken = false

    private var networkService = NetworkService()
    
    public init(appType: String, appToken: String, deviceId: String?) {
        self.appToken = appToken
        self.appType = appType
        let userId = UserCredentialStore().userCredentials?.userId
        analytics = FirebaseClient(appType: appType, userId: userId, deviceId: deviceId)
        waterIOClient = WaterIOClient(userId: userId, appType: appType, appToken: appToken)
        self.wioApi = WIOApi(waterIOClient: waterIOClient)
        self.sdkMessaging = SDKMessagingReport(apiManager: waterIOClient,
                                               networkManager: analytics)
        self.wioAnalytics = WIOAnalytics(analytics: analytics)
        self.appConfig = AppConfigService(waterIOClient: waterIOClient)
        self.appConfig.delegate = self
        self.wioApi.delegate = self
        self.wioApi.errorDelegate = self
        self.analytics.webService.delegate = self
    }
    
    func start() {
        self.wioApi.start(appToken: appToken)
        self.appConfig.start()
        self.sdkMessaging.start()
        networkService.startMonitoring()
        //networkService.onNetworkDidChanged = {[weak self] in }
    }
    
    func updateCapId(_ capId: String) {
        self.analytics.update(deviceId: capId)
    }
    
    public func sendAttribute(triggerEvent: String) {
        sdkMessaging.reportAttribut(triggerEvent: triggerEvent)
    }
}

extension WIONetworkManager: WIOApiDelegate, WIOApiErrorDelegate {
    public func userIdUpdated(_ userId: Int, json: [String: Any]) {
        //When user loggedin
        //analytics.resume(json: json)
        analytics.resume(userId: userId)
    }
    
    public func sendAllLogsFile() {
        sdkMessaging.sendLog(isSendAll: true)
    }
    
    public func serverError(error: WebserviceError) {
        sdkMessaging.serverError(error: error)
    }
    
    public func application(handleEventsForBackgroundURLSession identifier: String,
                            completionHandler: @escaping () -> Void) {
        if let uploadWebservice = analytics.webService {
            uploadWebservice.application(handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
        }
        
        if let uploadWebservice = waterIOClient.logUpload {
            uploadWebservice.application(handleEventsForBackgroundURLSession: identifier, completionHandler: completionHandler)
        }
    }
}

extension WIONetworkManager: AppConfigurationDelegate {
   
    public func onErrorRecived(_ error: [AppConfigError]) {
        
    }
    
    public func onAppConfigFinish() {
        analytics.resendMissingEvent()
        wioApi.start(appToken: appToken)
    }
}

extension WIONetworkManager: UploadWebserviceDelegate {
    public func needToUpdateAuthenticationToken() {
        refreshUserToken()
    }
    
    public func onEventUpload(_ event: String) {
        DDLogInfo("[SUCSESS] onEventUpload \(event)")
    }
    
    public func onEventFailedUpload(_ error: UploadWebserviceError, url: URL?) {
        switch error {
        case .notAuthenticated(let isTokenExipedTime):
            //if !isTokenExipedTime {
                //wioApi.sendEmail(error: error, url: url, title: "Upload event not authenticated")
                DDLogError("[ERROR] notAuthenticated \(error), \(String(describing: url)) isTokenExipedTime:\(isTokenExipedTime)")
            //}
        default:
            break
        }
        DDLogError("[ERROR] onEventFailedUpload \(error), \(String(describing: url))")
    }
    
    func refreshUserToken() {
        // FIXME: set isUpdateToken to flse after a when token will expired
        if isUpdateToken {
            return
        }
        
        isUpdateToken = true
        wioApi.refreshToken { [weak self] result in
            guard let me = self else { return }
            me.isUpdateToken = false
            switch result {
            case .success(let response):
                //DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                me.analytics.resume(json: response)
                //}
            case .error:
                DDLogError("[ERROR] refresh User Token")
            }
        }
    }
}
