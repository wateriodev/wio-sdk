//
//  WIOBle.swift
//  WioSDK
//
//  Created by israel on 30/04/2020.
//

import Foundation
import WaterIOBleKit

public protocol WIOBleManagerType {
    var capConnection: WIOCapConnection { get }
    var processor: WIOCapDataProcessor { get }
    var capCommandSender: WIOCapCommandSender { get }

    func getMacaddress() -> String?
    func getCapVersion() -> String?
    func getCapUUID() -> String?
}

typealias BLEAnalytics = BleManagerErrorDelegate & BleProcedureEventsDelegate & BleCommandsToCapDelegate

//bleManager.bleManagerErrorDelegate = self
//bleManager.bleCommandsToCapDelegate = self
//bleManager.bleProcedureEventsDelegate = self


public class WIOBleManager: WIOBleManagerType {
    
    internal var analyticsDelegates: BLEAnalytics?
    
    public let capConnection: WIOCapConnection
    public let processor: WIOCapDataProcessor
    public let capCommandSender: WIOCapCommandSender
    
    public var managerState: CBManagerState? {
        return bleManager.managerState 
    }
    
    //FIXME: should be private
    private var bleManager: BleManagerType

    public init(version: CapVersionType = .base) {
        bleManager = BleManager(capVersion: version)
        processor = WIOCapDataProcessor(bleManager: bleManager)
        capConnection = WIOCapConnection(bleManager: bleManager, version: version)
        capCommandSender = WIOCapCommandSender(bleManager: bleManager)
        bleManager.isAppInBackground = UIApplication.shared.applicationState == .background
        checkBleBackgrounPermission()
    }
    
    func start() {
        bleManager.initBluetoothProcess()
        capConnection.start()
        processor.start()
        capCommandSender.start()
    }

    private func checkBleBackgrounPermission() {
        let backgroundModes = Bundle.main.infoDictionary?["UIBackgroundModes"] as? [AnyHashable]
        if backgroundModes?.contains("bluetooth-central") == false ||
           backgroundModes?.contains("bluetooth-peripheral") == false {
            print("Background central support is not enabled. Add 'bluetooth-central' to UIBackgroundModes to enable background support")
        }
    }
    
    public func getMacaddress() -> String? {
        return CurrentDevice.currentDevice()?.MACAddress
    }
    
    public func getCapVersion() -> String? {
        return CapInfo.currentCap()?.capVersion
    }
    
    public func getCapUUID() -> String? {
        return CurrentDevice.currentDevice()?.UUID
    }
}
