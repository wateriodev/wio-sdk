//
//  BeaconController.swift
//  WaterIOBleKit
//
//  Created by israel on 23/04/2018.
//  Copyright © 2018 WaterIO. All rights reserved.
//

import UIKit
import CoreLocation
import UserNotifications
import CocoaLumberjack

public enum WIOBeaconState {
    case inside, outside, unknown
}

public protocol WIOBeaconControllerDelegate: class {
    func onBeaconDetected(_ state: WIOBeaconState, uuid: String)
    func locationManagerDidChangeAuthorization()
}
public extension WIOBeaconControllerDelegate {
    func locationManagerDidChangeAuthorization() {}
}

public class WIOBeaconController: NSObject {
    
    public static var shared = WIOBeaconController()
    
    public weak var delegate: WIOBeaconControllerDelegate?
    
    public var isMonitoring: Bool = false
    
    private var locationManager = CLLocationManager()
    public var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    lazy var beaconRegion: CLBeaconRegion = {
        let beacon = CLBeaconRegion(proximityUUID: WIOIBeacon.proximityUUID,
                                    major: WIOIBeacon.major,
                                    minor: WIOIBeacon.minor,
                                    identifier: WIOIBeacon.beaconID)
        beacon.notifyEntryStateOnDisplay = true
        beacon.notifyOnExit = true
        beacon.notifyOnEntry = true
        return beacon
    }()
    
    public func askLocationPermission() {
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.desiredAccuracy = .greatestFiniteMagnitude
        locationManager.requestAlwaysAuthorization()
        
        if !CLLocationManager.locationServicesEnabled() {
            DDLogError("locationServicesEnabled")
        }
        
        DDLogInfo("askLocationPermission \(CLLocationManager.locationServicesEnabled())")
    }
    
    public func startMonitoring() {
        DDLogInfo("startMonitoring")
        if authorizationStatus == .authorizedAlways {
            isMonitoring = true
        }
        locationManager.startMonitoring(for: beaconRegion)
    }
    
    public func stopMonitoring() {
        locationManager.stopMonitoring(for: beaconRegion)
    }
}

// MARK: CLLocationManagerDelegate
extension WIOBeaconController: CLLocationManagerDelegate {
    public func locationManagerDidChangeAuthorization(_ manager: CLLocationManager) {
        self.delegate?.locationManagerDidChangeAuthorization()
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didRangeBeacons beacons: [CLBeacon],
                                in region: CLBeaconRegion) {
        
        for beacon in beacons {
            sendNotification("locationManager didRangeBeacons for \(beacon.proximity)")
        }
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didDetermineState state: CLRegionState,
                                for region: CLRegion) {
        var newState: WIOBeaconState = .unknown
        switch state {
        case .inside:
            sendNotification("locationManager didDetermineState INSIDE for \(region.identifier)")
            newState = .inside
        case .outside:
            sendNotification("locationManager didDetermineState outside for \(region.identifier)")
            newState = .outside
        case .unknown:
            sendNotification("locationManager didDetermineState unknown for \(region.identifier)")
            newState = .unknown
        }
        
        self.delegate?.onBeaconDetected(newState, uuid: region.identifier)
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didEnterRegion region: CLRegion) {
        
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didEnterRegion \(region)")
        sendNotification("Enter Region")
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didExitRegion region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didExitRegion \(region)")
        sendNotification("Exit Region")
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didStartMonitoringFor region: CLRegion) {
        guard region is CLBeaconRegion else { return }
        DDLogInfo("locationManager didStartMonitoringFor \(region)")
        sendNotification("Start Monitoring")
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                monitoringDidFailFor region: CLRegion?,
                                withError error: Error) {
        DDLogInfo("locationManager monitoringDidFailFor \(region!)")
    }
    
    public func locationManager(_ manager: CLLocationManager,
                                didFailWithError error: Error) {
        DDLogInfo("locationManager monitoringDidFailFor \(manager)")
    }
}

extension WIOBeaconController {
    func sendNotification(_ body: String) {
        DDLogInfo("WIOBeaconController \(body)")
        
        //        WIOLocalNotification().scheduleNotification(notification: WIONotification(identifier: "123",
        //                          alertTitle: "iBeacon",
        //                          alertBody: body,
        //                          date: Date().addingTimeInterval(5),
        //                          repeats: .none))
    }
}


extension WIOBeaconController {
    func showLocationEnableAlert() {
        if let root = UIApplication.shared.keyWindow?.rootViewController {
            let alert = UIAlertController(title: "Your location service is off",
                                          message: "Please turn on Settings -> Privacy -> Location service", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .cancel, handler: nil))
            root.present(alert, animated: true, completion: nil)
        }
    }
}
