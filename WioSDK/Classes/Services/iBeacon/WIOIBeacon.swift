//
//  WIOIBeacon.swift
//  WioSDK
//
//  Created by israel water-io on 25/10/2020.
//

import Foundation
import CoreLocation

struct WIOIBeacon {
    //Ex: C37D274C-CD12-44F6-B970-7364B4958B83
    //Water-io: 9930701F-1764-426F-8616-4589B87C192F
    static let proximityUUID = UUID(uuidString:"9930701F-1764-426F-8616-4589B87C192F")!
    static let major: CLBeaconMinorValue = 0
    static let minor: CLBeaconMinorValue = 0
    static let beaconID = "com.waterio"
}
