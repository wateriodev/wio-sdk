import Foundation
import CoreLocation
import WaterIONetwork

public enum LocationType {
    case always
    case whenInUse
}

extension CLAuthorizationStatus {
    var description: String {
        switch self {
        case .denied, .restricted, .notDetermined:
            return "NO"
        case .authorizedAlways:
            return "ALWAYS"
        case .authorizedWhenInUse:
            return "WHILE_USING_APP"
        @unknown default:
            return "NO"
        }
    }
}

final class WIOLocationService: NSObject {
    
    static var shared = WIOLocationService()
        
    var locationServiceDidGetLocation: ((Error?) -> Void)?
    
    var failedToGetLocationByDevice: Bool?
    
    var locationType: LocationType = .whenInUse
    
   
    var locationServicesEnabled: Bool {
        return CLLocationManager.locationServicesEnabled()
    }
    
    var authorizationStatus: CLAuthorizationStatus {
        return CLLocationManager.authorizationStatus()
    }
    
    fileprivate var locationManager: CLLocationManager?
    
    public var currentUserLocation: CLLocationCoordinate2D?
       
    
    func requestUserLocation(locationType: LocationType) {
        if locationServicesEnabled == false {
            failedToGetLocationByDevice = true
            return
        }
        
        if locationManager == nil {
            locationManager = CLLocationManager()
            locationManager?.delegate = self
            locationManager?.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        }
        requestLocation(locationType: locationType)
    }
    
    fileprivate func requestLocation(locationType: LocationType) {
        //TODO: - check the plist text permission
        switch locationType {
        case .always:
            locationManager?.requestAlwaysAuthorization()
        case .whenInUse:
            locationManager?.requestWhenInUseAuthorization()
        }
    }
    
    fileprivate func notifyCompletion(with error: Error?) {
        locationServiceDidGetLocation?(error)
        locationServiceDidGetLocation = nil
    }
}

// MARK: - CLLocationManagerDelegate
extension WIOLocationService: CLLocationManagerDelegate {
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        
        switch status {
        case .denied:
            manager.delegate = nil
            failedToGetLocationByDevice = true
            notifyCompletion(with: nil)
            //getLocationByIP()
        case .authorizedAlways:
            manager.requestAlwaysAuthorization()
            self.locationType = .always
        case .authorizedWhenInUse:
            manager.requestLocation()
            self.locationType = .whenInUse
        case .notDetermined:
            if authorizationStatus == .authorizedAlways {
                requestLocation(locationType: .always)
                return
            }
            requestLocation(locationType: .whenInUse)
        case .restricted:
            break
         default:
            break
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if locationType == .whenInUse {
            manager.delegate = nil
        }
        
        guard let loc = locations.first?.coordinate else {
            return
        }
        
        currentUserLocation = loc
        failedToGetLocationByDevice = false
        notifyCompletion(with: nil)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        manager.delegate = nil
        failedToGetLocationByDevice = true
    }
}
