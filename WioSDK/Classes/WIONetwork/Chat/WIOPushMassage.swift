//
//  Massage.swift
//  WioSDK
//
//  Created by israel on 09/12/2019.
//

import Foundation

enum MassageState: String {
    case send = "send"
    case read = "read"
    case received = "received"
    case sent = "sent"
    case pending = "pending"
}

struct WIOPushMassage {
    public let timestamp: Int
    public let text: String
    public let id: String
    public let status: MassageState?
    public let senderName: String?

    public init(timestamp: Int, text: String, status: MassageState = MassageState.pending , senderName: String? = "user") {
        self.timestamp = timestamp
        self.text = text
        self.status = status
        self.id = "\(WIOPushMassage.getId(timestamp: timestamp)) \(text)"
        self.senderName = senderName
    }
    
    public init(json: [String: Any], id: String) {
        self.timestamp = json["event_time"] as! Int
        self.text = json["text"] as! String
        self.status = MassageState(rawValue: json["status"] as! String)
        self.senderName = json["sent_by"] as? String
        self.id = id
    }
    
    static func convertToDictionary(from text: String) throws -> [String: Any] {
        guard let data = text.data(using: .utf8) else { return [:] }
        let anyResult: Any = try JSONSerialization.jsonObject(with: data, options: [])
        return anyResult as? [String: Any] ?? [:]
    }
    
    static func getId(timestamp: Int) -> String {
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        let date = Date(timeIntervalSince1970: TimeInterval(timestamp))
        return dateFormatterGet.string(from: date)
    }
}
