import Foundation
import WaterIONetwork
import CocoaLumberjack

protocol WIOChatManagerDelegate: class {
    func onRecivedMessage(massages: [WIOPushMassage])
}

class WIOChatManager {
    
    weak var delegate: WIOChatManagerDelegate?
    var massages: [WIOPushMassage] = []
    
    private let waterIOClient: WaterIOClient

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    init(waterIOClient: WaterIOClient) {
        self.waterIOClient = waterIOClient
        registerNotificaiton()
    }
    
    func updateMessage(with id: String, status: MassageState) {
        updateMessage(id: id, status: status)
    }
    
    func sendMessage(_ text: String) {
        let currentTime = Int(Date().timeIntervalSince1970)
        let message = WIOPushMassage(timestamp: currentTime,
                                     text: text,
                                     status: .pending)
        massages.append(message)
        sendMessage(id: message.id, text: message.text)
    }
    
    func getAllMessages() -> [WIOPushMassage] {
        return massages
    }
    
    func onRecivedMessage(userInfo: [AnyHashable : Any]?) {
        guard let json = userInfo as? [String: Any],
            let messages = json["messages"] as? String,
            let map = try? WIOPushMassage.convertToDictionary(from: messages) else {
                fatalError("Invalid data")
        }
        
        guard let id = map.keys.first,
            let value = map.values.first as? [String : Any] else {
                fatalError("Parsing failed")
        }
        
        let message = WIOPushMassage(json: value, id: id)
        massages.append(message)
        self.delegate?.onRecivedMessage(massages: [message])
    }
    
    func update() {
        DDLogInfo("update")
    }
}

//MARK: API
extension WIOChatManager {
    
    //TODO: change @escaping To Result
//    public func getAllMissingMessage(completion: @escaping ([WIOPushMassage]) -> Void) {
//        waterIOClient.getAllMissingMessage { result in
//            switch result {
//            case .success(let result):
//                DDLogInfo("getAllMissingMessage" + "\(result)")
//                guard let convert = result as? [String: [String: [String: Any]]] else {
//                    return
//                }
//
//                var messages: [WIOPushMassage] = []
//                for (key, value) in convert {
//                    DDLogInfo(key)
//                    for (subKey, subValue) in value {
//                        messages.append(WIOPushMassage(json: subValue, id: subKey))
//                    }
//                }
//                completion(messages)
//            case .error(let error):
//                completion([])
//                DDLogInfo("getAllMissingMessage error" + "\(error)")
//
//            }
//        }
//    }
    
    func sendMessage(id: String, text: String) {
        waterIOClient.sendMessage(id: id, text: text) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let result):
                DDLogInfo("sendMessage" + "\(result)")
                me.update()
            case .error(let error):
                DDLogInfo("sendMessage error" + "\(error)")
            }
        }
    }
    
    func updateMessage(id: String, status: MassageState) {
        waterIOClient.updateMessage(id: id, status: status.rawValue) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let result):
                DDLogInfo("updateMessage" + "\(result)")
                me.update()
            case .error(let error):
                DDLogInfo("updateMessage error" + "\(error)")
                
            }
        }
    }
}

extension WIOChatManager {
    private func registerNotificaiton() {
//        NotificationCenter.default.addObserver(
//            self,
//            selector: #selector(self.onRecivedMessageObserver),
//            name: NSNotification.Name(rawValue: WIOConstance.onPushNotificaitonRecived),
//            object: nil)
    }
    
    @objc private func onRecivedMessageObserver(notification: NSNotification) {
        if let objcet = notification.object as? [AnyHashable : Any] {
            onRecivedMessage(userInfo: objcet)
        } else {
            assert(true, "Message is empty")
        }
    }
}
