import Foundation
import CocoaLumberjack

internal struct LogConstants {
    static let TempDirectoryName = "com.waterio.ios.logs/"
    static let FileExtension = "zip"
}

class LogFile: NSObject {
    
    internal let fileHelper = LogFileHelper()

    public var dateFormatter: DateFormatter {
        let formmater = DateFormatter()
        formmater.dateFormat = "yyyy-MM-dd"
        return formmater
    }
    
    private lazy var fileName: String = {
        return dateFormatter.string(from: Date()) //+ "_\(UUID().uuidString)"
    }()
    
    public func isBeforeToday(fileName: String) -> Bool {
        guard let dateString = fileName.split(separator: "_").first,
            let date = dateFormatter.date(from: String(dateString)) else {
                return true
        }
        return date.startOfDay.daysFromToday() > 0
    }
    
    private lazy var tasksDirectory: URL = {
        let cachesDirectoryPath = NSSearchPathForDirectoriesInDomains(.documentDirectory, .userDomainMask, true)[0]
        let cachesDirectoryURL = URL(fileURLWithPath: cachesDirectoryPath)
        let folderDirectory = LogConstants.TempDirectoryName
        let directoryURL = cachesDirectoryURL.appendingPathComponent(folderDirectory)
        return directoryURL
    }()
    
    override init() {
        super.init()
        fileHelper.createDirectory(url: tasksDirectory)
    }
    
    func save(json: [String: Any]) {
        let directoryURL = tasksDirectory
        let fileURL = directoryURL.appendingPathComponent(fileName).appendingPathExtension(LogConstants.FileExtension)
        do {
            try fileHelper.writeEncoded(JSON: json, to: fileURL)
        } catch {
            DDLogInfo("exception: \(error)")
        }
    }
    
    func getAllLogJson() -> [(fileName: String, url: URL)]? {
        guard let files = try? LogFileHelper().readJsonFile(tasksDirectory,
                                                            fileType: LogConstants.FileExtension) else {
                                                                return nil
        }
        
        return files.map{(fileName: $0.deletingPathExtension().lastPathComponent, url: $0)}
    }
    
    func readFile(url: URL) -> String? {
        guard let context = try? String(contentsOf: url) else {
            return nil
        }
        
        return context
    }
    
    func readJsonFile(_ dierctory: URL) throws -> [String: Any] {
        let jsonData = try Data(contentsOf: dierctory)
        do {
            let jsonResult: NSDictionary = try
                JSONSerialization.jsonObject(with: jsonData,
                                             options: JSONSerialization.ReadingOptions.mutableContainers) as! NSDictionary
            return jsonResult as! [String : Any]
        }
    }
    
    func readDataFile(_ dierctory: URL) throws -> Data {
        return try Data(contentsOf: dierctory)
    }
    
    func removeFile(_ url: URL) {
        fileHelper.removeFile(url: url)
    }
}

extension Date {
    
    func daysFromToday() -> Int {
        return abs(Calendar.current.dateComponents([.day], from: self, to: Date()).day!)
    }
    
    var startOfDay: Date {
        return Calendar.current.startOfDay(for: self)
    }
    
    var tomorrow: Date {
       return Calendar.current.date(byAdding: .day, value: 1, to: startOfDay)!
    }
    
    var endOfDay: Date {
        var components = DateComponents()
        components.day = 1
        components.second = -1
        return Calendar.current.date(byAdding: components, to: startOfDay)!
    }
}
