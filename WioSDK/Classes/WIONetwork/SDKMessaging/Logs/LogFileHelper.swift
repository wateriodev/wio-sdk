import Foundation
import CocoaLumberjack

enum LogFileError: Error {
    case encodeDataFileAlreadyExist
    case encodeDataURLInvalid(fileURL: URL)
    case encodeDataToJSONSerializationFailed
}

final class LogFileHelper {
    
    private let fileManager = FileManager.default
    private let queue = DispatchQueue(label: "com.waterio.wirteLog." + UUID().uuidString)
    private let csvHeader: [String] = ["event_type", "event_time", "local_time", "json"]
    
    private lazy var dateFormatter: DateFormatter = {
        let timeformmater = DateFormatter()
        timeformmater.dateFormat = "yyyy-MM-dd hh:mm:ss"
        return timeformmater
    }()
    
    func createCsvHeader() -> String {
        let createHeader = csvHeader.joined(separator: ",")
        return createHeader + "\n"
    }
    
    func getLocalTime(timestemp: Int) -> String {
        let date = Date(timeIntervalSince1970: TimeInterval(timestemp))
        return dateFormatter.string(from: date)
    }
    
    func appendToCsv(JSON: [String: Any]) -> String {
        guard let eventType = JSON["event_type"] as? String,
            let eventTime = JSON["event_time"] as? Int else {
                fatalError("")
        }
        
        let eventTimeString = "\(eventTime)"
        let jsonData = JSON.description.replacingOccurrences(of: ",", with: " ", options: NSString.CompareOptions.literal, range: nil)
        let localTime = getLocalTime(timestemp: eventTime)
        
        let data = [eventType, eventTimeString, localTime, jsonData].joined(separator: ",")
        return data + "\n"
    }
    
    @discardableResult
    func writeEncoded(JSON: [String: Any], to fileURL: URL) throws -> Int {
       
        if !fileURL.isFileURL {
            throw LogFileError.encodeDataURLInvalid(fileURL: fileURL)
        }
       
        guard let data = appendToCsv(JSON: JSON).data(using: .utf8) else {
            throw LogFileError.encodeDataURLInvalid(fileURL: fileURL)
        }
        
        let isFileExists = FileManager.default.fileExists(atPath: fileURL.path)
        
        if  isFileExists {
            if let fileHandle = try? FileHandle(forWritingTo: fileURL) {
                fileHandle.seekToEndOfFile()
                fileHandle.write(data)
                fileHandle.closeFile()
            }
            return data.count
        }
        
        guard let header = createCsvHeader().data(using: .utf8) else {
            throw LogFileError.encodeDataURLInvalid(fileURL: fileURL)
        }
       
        let headerData = header + data
        
        queue.sync {
            do {
                try headerData.write(to: fileURL, options: Data.WritingOptions())
            } catch {
                DDLogInfo("failure To cretae wirte at: \(fileManager)")
            }
        }
        return data.count
    }
    
    func createDirectory(url: URL) {
        let fileManager = FileManager.default
        queue.sync {
            do {
                try fileManager.createDirectory(at: url,
                                                withIntermediateDirectories: true, attributes: nil)
            } catch {
                DDLogError("failure To cretae Folder at: \(fileManager)")
            }
        }
    }
    
    func removeFile(url: URL) {
        try? fileManager.removeItem(at: url)
    }
    
    func readJsonFile(_ url: URL, fileType: String) throws -> [URL] {
        let dirEnumerator = FileManager.default.enumerator(at: url, includingPropertiesForKeys: [.nameKey, .isDirectoryKey], options: FileManager.DirectoryEnumerationOptions.skipsSubdirectoryDescendants, errorHandler: sendError)
        var theArray: [URL] = []

        if let dirEnumerator = dirEnumerator {
            for theURL in dirEnumerator {
                guard let theURL = theURL as? URL else {
                    continue
                }

                if theURL.pathExtension == fileType {
                    theArray.append(theURL)
                }
            }
        }
        return theArray
    }
    
    
    func sendError(url: URL, error: Error) -> Bool {
        DDLogInfo("ERROR READ file \(url) \(error)")
        return true
    }
}
