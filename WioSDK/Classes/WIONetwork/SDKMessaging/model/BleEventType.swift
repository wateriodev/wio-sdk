//
//  BleEventType.swift
//  WioSDK
//
//  Created by israel on 22/04/2020.
//

 
 
 import Foundation
import WaterIOBleKit

enum BleEventType {
    case startConnectionProcedure
    case discoveredDevice(String)
    case connectedToDevice
    case scanDeviceEndedWithNoResults
    case discoveredServices(String)
    case discoveredCharacteristics(String)
    case registeredToNotifications
    case forgetDevice
    case startScan
    case stopScan
    
    case capLogs(text: String)
    case capEvent(text: String)
    case capCommand(name: String, time: Int)
    
    static var all: [BleEventType] {
        return [
            .startConnectionProcedure,
            .discoveredDevice("fake"),
            .connectedToDevice,
            .scanDeviceEndedWithNoResults,
            .discoveredServices("fake"),
            .discoveredCharacteristics(""),
            .registeredToNotifications,
            .forgetDevice,
            .startScan,
            .stopScan,
            .capEvent(text: "f_open"),
            .capLogs(text: "f_open"),
            .capCommand(name: "f_get_log", time: 1)
        ]
    }
    
    var logLevel: DebugLevel {
        switch self {
        case .discoveredDevice, .startScan,
             .discoveredServices, .discoveredCharacteristics,
             .registeredToNotifications, .stopScan, .capCommand:
            return .Low
        case .capLogs:
            return .High
        default:
            return .Low
        }
    }
    
    var typeName: String {
        switch self {
        case .startScan:
            return "start_scan"
        case .stopScan:
            return "stop_scan"
        case .startConnectionProcedure:
            return "start_connection_procedure"
        case .discoveredDevice:
            return "discovered_device"
        case .connectedToDevice:
            return "connected_to_device"
        case .scanDeviceEndedWithNoResults:
            return "scan_device_ended_with_no_results"
        case .discoveredServices:
            return "discovered_services"
        case .discoveredCharacteristics:
            return "discovered_characteristics"
        case .registeredToNotifications:
            return "registered_to_notifications"
        case .forgetDevice:
            return "forget_device"
        case .capEvent(let text):
            return text
        case .capCommand(let name, _):
            return name
        case .capLogs(let text):
            return text
        }
    }
    
    var description: String {
        switch self {
        case .startConnectionProcedure:
            return "Start connection procedure"
        case .discoveredDevice(let devices):
            return "Discovered device " + devices
        case .connectedToDevice:
            return "Connected to device"
        case .scanDeviceEndedWithNoResults:
            return "Scan device ended with no results"
        case .discoveredServices(let services):
            return "Discovered services " + services
        case .discoveredCharacteristics(let characteristics):
            return "Discovered characteristics " + characteristics
        case .registeredToNotifications:
            return "Registered to notifications"
        case .forgetDevice:
            return "Forget device"
        case .capEvent(let text):
            return text
        case .capLogs(let text):
            return text
        case .capCommand(let name, _):
            return name
        case .startScan:
            return "Start scan"
        case .stopScan:
            return "Stop scan"
        }
    }
}

extension CapEventEntry {
    func getCode() -> String {
        return code.binary
    }
    
    func getEventText() -> String {
        let text = getReportName().replacingOccurrences(of: "_", with: " ")
        return text
    }
    
    func getReportName() -> String {
        switch opCode {
        case .appConnected:
            return "cap_connected"
        case .appDisconnected:
            return "cap_disconnected"
        case .length, .measure, .firstMeasure:
            return "measurement"
        case .unknownZ:
            return "cap_reset"
        case .voltage:
            return "cap_voltage"
        case .preRemind:
            return "pre_reminder"
        case .vitaminsReminder, .remind:
            return "reminder"
        case .open:
            return "open"
        case .close:
            return "close"
        default:
            return code.unicode
        }
    }
}


extension CapResponseAPI {
    
    var typeName: String {
        switch self {
        case .ack:
            return "ack"
        case .sendRtc:
            return "send_rtc"
        case .sendVersion:
            return "send_version"
        case .getMACAddress:
            return "getMACAddress"
        case .reset:
            return "reset"
        default:
            return rawValue.binary
        }
    }
}
extension CapCommandAPI {
    var eventText: String {
        return typeName.replacingOccurrences(of: "_", with: " ")
    }
    
    var typeName: String {
        switch self {
        case .playSound:
            return "play_sound"
        case .getLogLength:
            return "get_log_length"
        case .getLog://CHECK
            return "get_log"
        case .clearLog:
            return "clean_log"
        case .setRtc:
            return "set_rtc"
        case .setSleep:
            return "set_sleep"
        case .getVersion:
            return "cap_version"
        case .setBlinkingTime:
            return "set_blinking_time"
        case .runBlinking:
            return "run_blinking"
        case .setDailyUsage:
            return "set_daily_usage_time"
        case .setVitaminsReminderTime:
            return "set_vitamins_reminder_time"
        case .enterOTA:
            return "start_ota"
        case .reminders(let time):
            return "reminders_\(time)"
        default:
            return self.opCode.binary
        }
    }
}

extension UInt8 {
    var binary: String {
        return "\(self)"
    }
    var unicode: String {
        //let hexString = String(format:"%llX", self)
        let hexString =  "\(Character(UnicodeScalar(self)))"
        return hexString
    }
}
