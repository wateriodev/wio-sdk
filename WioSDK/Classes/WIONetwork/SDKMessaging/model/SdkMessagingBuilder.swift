//
//  SdkMessagingBuilder.swift
//  CocoaLumberjack
//
//  Created by israel on 27/04/2020.
//

import Foundation
import WaterIONetwork

struct BleErrorReport: SdkMessaging {
    var logLevel: DebugLevel = .High
    
    var reportType: ReportType = ReportType.error
    var date: Date
    let type: BleErrorType
    let deviceId: String
    
    func toJSON() -> [String : Any] {
        return [
            "event_time": Int(date.timeIntervalSince1970),
            "event_type" : type.typeName,
            "time" : fullTimestamp(from: date),
            "device_id" : deviceId]
    }
}

struct BleEventReport: SdkMessaging {
   
    var logLevel: DebugLevel  {
        return type.logLevel
    }
    
    var reportType: ReportType = ReportType.event
    
    var date: Date
    let type: BleEventType
    let deviceId: String
    let data: Any?
    let text: String
    let timestamp: Int
    init(date: Date = Date(), type: BleEventType, deviceId: String, data: Any?, text: String, timestamp: Int? = nil) {
        self.timestamp = timestamp ?? Int(date.timeIntervalSince1970)
        self.date = date
        self.type = type
        self.deviceId = deviceId
        self.data = data
        self.text = text
    }
    
    func toJSON() -> [String : Any] {
        return [
            "event_type": type.typeName,
            "event_time": timestamp,
            //"event_desc": text,
            "event_data" : data ?? "",
            "event_source": "cap",
            //"event_text": text,
            "device_id" : deviceId]
    }
}

struct NetworkErrorReport: SdkMessaging {
    var logLevel: DebugLevel = DebugLevel.High
    
    var date: Date
    let type: NetworkErrorType
    let url: URL
    var reportType: ReportType = ReportType.error
    var data: Any?
    
    func toJSON() -> [String : Any] {
        
        return [
            "url": url.absoluteString,
            "event_type" : type.typeName,
            "event_time": Int(date.timeIntervalSince1970),
            "event_data" : data ?? "",
            "network" : getNetwerkType()]
    }
}
