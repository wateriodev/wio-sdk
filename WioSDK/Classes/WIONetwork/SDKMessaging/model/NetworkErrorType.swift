//
//  NetworkErrorType.swift
//  WioSDK
//
//  Created by israel on 22/04/2020.
//

import Foundation
import WaterIONetwork

enum NetworkErrorType {
    case serverReponse(Int)
    case timeout
    case networkError
    
    var typeName: String {
        switch self {
        case .serverReponse:
            return "server_reponse"
        case .timeout:
            return "timeout"
        case .networkError:
            return "network_error"
        }
    }
    
    var description: String {
        switch self {
        case .serverReponse(let code):
            return "Server reponse \(code)"
        case .timeout:
            return "timeout"
        case .networkError:
            return "network Error"
        }
    }
}
