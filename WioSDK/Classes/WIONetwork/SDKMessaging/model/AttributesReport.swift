//
//  DeviceInfo.swift
//  CocoaLumberjack
//
//  Created by israel on 19/04/2020.
//

import Foundation
import WaterIONetwork
import WaterIOBleKit

public struct AttributesReport: SdkMessaging {
    var logLevel: DebugLevel = DebugLevel.High
    
    var reportType: ReportType = ReportType.attribut
    let SDKversion: String = "1.0.1"
    var date: Date
    let appType: String
    let appToken: String
    let osType: String
    let osVersion: String
    let phoneModel: String
    let hostAppVersion: String
    let userLocale: String
    let geoLocation: String
    let ipAddress: String
    let phoneId: String
    let timeZone: String
    let uid: Int?
    let eventTime: Int
    let isPermissionBluetoothIos: Bool
    let isPermissionNotification: Bool
    let permissionGeolocation: String
    let isBluetoothOn: Bool
    let isIbeaconOnIos: Bool
    let triggerEvent: String
    
    private let currentDevice = UIDevice.current
    
    var description: String {
        return self.toJSON().description
    }
    
   
    init(appType: String,
         appToken: String,
         uid: Int?,
         //Logging
         isPermissionBluetoothIos: Bool,
         isPermissionNotification: Bool,
         permissionGeolocation: String,
         isBluetoothOn: Bool,
         isIbeaconOnIos: Bool,
         triggerEvent: String) {
    
    
        self.isPermissionBluetoothIos = isPermissionBluetoothIos
        self.isPermissionNotification = isPermissionNotification
        self.permissionGeolocation = permissionGeolocation
        self.isBluetoothOn = isBluetoothOn
        self.isIbeaconOnIos = isIbeaconOnIos
        
        self.triggerEvent = triggerEvent
        
        
        //self.SDKversion = SDKversion
        self.appType = appType
        self.appToken = appToken
        self.osType = currentDevice.systemName
        self.osVersion = currentDevice.systemVersion
        self.phoneModel = currentDevice.model
        self.hostAppVersion = AttributesReport.getHostAppVersion()
        self.userLocale = Locale.current.identifier
        self.geoLocation = ""
        self.ipAddress = currentDevice.getIPAddress()
        self.phoneId = currentDevice.identifierForVendor?.uuidString ?? "UNKNOW"
        self.timeZone = TimeZone.current.identifier
        self.uid = uid
        self.eventTime = Int(Date().timeIntervalSince1970)
        self.date = Date()
    }
//    
//    public init() {
//        //self.SDKversion = SDKversion
//        self.appType = WIOApp.shared.appType
//        self.appToken = WIOApp.shared.appToken
//        self.osType = currentDevice.systemName
//        self.osVersion = currentDevice.systemVersion
//        self.phoneModel = currentDevice.model
//        self.hostAppVersion = AttributesReport.getHostAppVersion()
//        self.userLocale = Locale.current.identifier
//        self.geoLocation = ""
//        self.ipAddress = currentDevice.getIPAddress()
//        self.phoneId = currentDevice.identifierForVendor?.uuidString ?? "UNKNOW"
//        self.timeZone = TimeZone.current.identifier
//        self.uid = 0
//        self.eventTime = Int(Date().timeIntervalSince1970)
//        self.date = Date()
//
//
//        self.isPermissionBluetoothIos = false//WIOApp.shared.bleMenegaer.managerState != .unauthorized
//        self.isPermissionNotification = false
//        self.permissionGeolocation = WIOLocationService().authorizationStatus.description
//        self.isBluetoothOn = false//WIOApp.shared.bleMenegaer.managerState == .poweredOn
//        self.isIbeaconOnIos = false
//        self.triggerEvent = "Error"
//    }
//    
    
    public func toJSON() -> [String: Any] {
        var JSON: [String: Any] = [
            "event_type": "attribute",
            "event_time" : eventTime,
            "event_data" : "",
            "app_type": appType,
            "os_type" : osType,
            "os_version": osVersion,
            "sdk_version": SDKversion,
            "app_version": hostAppVersion,
            "phone_version": UIDevice.modelName,
            "phone_model": phoneModel,
            "phone_id": phoneId,
            "ip": ipAddress,
            "language_code": languageCode,
            "timezone": getTimeZone(),
            "device_id": deviceUUID,
            "network": getNetwerkType(),
            "is_permission_bluetooth_ios": isPermissionBluetoothIos,
            "is_permission_notification": isPermissionNotification,
            "permission_geolocation": permissionGeolocation,
            "is_bluetooth_on": isBluetoothOn,
            "is_ibeacon_on_ios": isIbeaconOnIos,
            "status_battery": batteryLevel,
            "is_plugged_to_charger": isPluggedToCharger,
            "is_low_power_mode": isLowPowerMode,
            "trigger_event": triggerEvent,
            "extra_attr1": "",
            "extra_attr2": "",
            "extra_attr3": ""]
        
        
        if let location = getLoaction() {
            JSON["latitude"] = location.lat
            JSON["longitude"] = location.lon
        }
        return JSON
    }
    
}

extension AttributesReport {
    
    var deviceUUID: String {
        var fullId = ""
        if let uuid = CurrentDevice.currentDevice()?.UUID {
            fullId += uuid
        }
        if let macAddress = CurrentDevice.currentDevice()?.MACAddress {
            fullId += "_\(macAddress)"
        }
        return fullId
    }
    
     var isLowPowerMode: Bool {
        return ProcessInfo.processInfo.isLowPowerModeEnabled
    }
    
     var batteryLevel: Int {
        currentDevice.isBatteryMonitoringEnabled = true

        let batteryLevel = UIDevice.current.batteryLevel
        if batteryLevel < 0.0 {
            print(" -1.0 means battery state is UIDeviceBatteryStateUnknown")
            return 0
        }
        
        let percentage = batteryLevel * 100
        print("Battery level is \(batteryLevel)")
        currentDevice.isBatteryMonitoringEnabled = false
        return Int(percentage)
    }
    
     var isPluggedToCharger: Bool {
        currentDevice.isBatteryMonitoringEnabled = true
        let state = UIDevice.current.batteryState
        switch  state {
            case .unplugged, .unknown:
                currentDevice.isBatteryMonitoringEnabled = false
                return false
            case .charging, .full:
                currentDevice.isBatteryMonitoringEnabled = false
                return true
         default:
            currentDevice.isBatteryMonitoringEnabled = false
            return false
        }
    }
    
    var languageCode: String {
        return Locale.current.languageCode ?? "" // App lang
    }
    
    static func getHostAppVersion() -> String  {
        guard let info = Bundle.main.infoDictionary else { return "" }
        let appVersion = info["CFBundleShortVersionString"] as? String ?? "Unknown"
        let appBuild = info[kCFBundleVersionKey as String] as? String ?? "Unknown"
        return "\(appVersion).\(appBuild)"
    }
    
    func getLoaction() -> (lat: Double, lon: Double)? {
//        if let location = LocationService().currentUserLocation {
//            return (lat: location.latitude, lon: location.longitude)
//        }
        return nil
    }
    
    func getTimeZone() -> String {
        if let time = TimeZone.current.abbreviation() {
            return time
        }
        return "UNKNOW"
    }
    
    func getNetwerkType() -> String {
        return NetworkService().currntStatus.status
    }
}
