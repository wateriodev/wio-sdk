//
//  ErrorReport.swift
//  CocoaLumberjack
//
//  Created by israel on 20/04/2020.
//

import Foundation
import WaterIONetwork

enum ReportType {
    case error
    case event
    case attribut
}

protocol SdkMessaging {
    var reportType: ReportType { get }
    var logLevel: DebugLevel { get }
    var date: Date { get set }
    
    func toJSON() -> [String: Any]
}


extension SdkMessaging {
    func fullTimestamp(from date: Date) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        return dateFormatter.string(from: date)
    }
    
    func getNetwerkType() -> String {
        return NetworkService().currntStatus.status
    }
}

extension NetworkType {
    var status: String {
        switch self {
        case .G2:
            return "2G"
        case .G3:
            return "3G"
        case .G4:
            return "4G"
        case .wifi:
            return "WiFi"
        case .notConnected:
            return "UNKNOW"
        case .unknow:
            return "UNKNOW"
        }
    }
}
