//
//  BleErrorType.swift
//  WioSDK
//
//  Created by israel on 22/04/2020.
//

import Foundation

enum DebugLevel {
    case High //Send to server
    case Medium
    case Low // Wirte in file
}

enum BleErrorType {
    
    case noBluetoothPermission
    case bluetoothIsTurnedOff
    case failedToConnect
    case discoverServicesFailure
    case discoverCharacteristicsFailure
    case registerToNotificationsFailure
    case bleAdapterError
    case noDeviceResponseCommand
    case deviceNack
    case bleConnectionListen

    var logLevel: DebugLevel {
       return .Low
    }
    
    var typeName: String {
        switch self {
        case .noBluetoothPermission:
            return "no_bluetooth_permission"
        case .bluetoothIsTurnedOff:
            return "bluetooth_is_turned_off"
        case .failedToConnect:
            return "failed_to_connect"
        case .discoverServicesFailure:
            return "discover_services_failure"
        case .discoverCharacteristicsFailure:
            return "discover_characteristics_failure"
        case .registerToNotificationsFailure:
            return "register_to_notifications_failure"
        case .bleAdapterError:
            return "ble_adapter_error"
        case .noDeviceResponseCommand:
            return "no_device_response_command"
        case .deviceNack:
            return "device_nack"
        case .bleConnectionListen:
            return "connection_listener"
        }
    }
    
    static var all: [BleErrorType] {
        return [
            .noBluetoothPermission,
            .bluetoothIsTurnedOff,
            .failedToConnect,
            .discoverServicesFailure,
            .discoverCharacteristicsFailure,
            .registerToNotificationsFailure,
            .bleAdapterError,
            .noDeviceResponseCommand,
            .bleConnectionListen,
            .deviceNack]
    }
}
