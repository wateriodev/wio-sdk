import Foundation
import WaterIONetwork

extension SDKMessagingReport: WIOApiErrorDelegate {
    func serverError(error: WebserviceError) {
        switch error {
        case .notAuthenticated(let url):
            reportError(url: url, code: 401)
        case .other(let url):
            reportNetworkError(url: url)
        case .parsingFailed(let url):
            reportError(url: url, code: 10000)
        case .serverError(let statusCode, let url):
            reportError(url: url, code: statusCode)
        case .emptyResponse(let url):
            reportError(url: url, code: 0)
        case .timeout(let url):
            reportError(url: url, code: 100)
        case .networkError(let url):
            reportNetworkError(url: url)
        }
    }
    
    func reportError(url: URL?, code: Int) {
        guard let url = url else {
            return
        }
        
        let messaging  = NetworkErrorReport(date: Date(),
                                            type: .serverReponse(code),
                                            url: url,
                                            data: code)
        report(messaging: messaging)
    }
    
    func reportNetworkError(url: URL?) {
        guard let url = url else {
            return
        }
        
        let messaging  = NetworkErrorReport(date: Date(),
                                            type: .networkError,
                                            url: url)
        report(messaging: messaging)
    }
}

extension SDKMessagingReport {
    func onErrorRecived(url: URL, error: Error) {
        let messaging  = NetworkErrorReport(date: Date(),
                                            type: .networkError,
                                            url: url)
        report(messaging: messaging)
    }
}

