import Foundation
import WaterIOBleKit
import CocoaLumberjack

extension SDKMessagingReport: BleManagerErrorDelegate {
    func onDeviceConnectedListenerEmpty() {
        //reportBleError(errorType: .)
        let messaging = BleErrorReport(date: Date(),
                                       type: .bleConnectionListen,
                                       deviceId: macAddress)
        report(messaging: messaging)
    }
    
    func failedToConnect(deviceUUID: String, error: Error?) {
        reportBleError(errorType: .failedToConnect)

    }
    
    func reportBleError(errorType: BleErrorType) {
        let messaging = BleErrorReport(date: Date(),
                                       type: errorType,
                                       deviceId: macAddress)
        report(messaging: messaging)
    }
    
    func noBluetoothPermission() {
        reportBleError(errorType: .noBluetoothPermission)
    }
    
    func bluetoothIsTurnedOff() {
        reportBleError(errorType: .bluetoothIsTurnedOff)
    }
    
    func discoverServicesFailure(error: Error) {
        reportBleError(errorType: .discoverServicesFailure)
    }
    
    func discoverCharacteristicsFailure(error: Error) {
        reportBleError(errorType: .discoverCharacteristicsFailure)
    }
    
    func registerToNotificationsFailure(error: Error) {
        reportBleError(errorType: .registerToNotificationsFailure)
    }
    
    func BLEAdapterError(error: Error) {
        reportBleError(errorType: .bleAdapterError)
    }
    
    func noDeviceResponseForCommand() {
        reportBleError(errorType: .noDeviceResponseCommand)
    }
    
    func deviceNack() {
        reportBleError(errorType: .deviceNack)
    }
}

extension SDKMessagingReport: BleProcedureEventsDelegate {
    
    func reportBleEvent(eventType: BleEventType) {
        let messaging = BleEventReport(date: Date(),
                                       type: eventType,
                                       deviceId: macAddress,
                                       data: 0,
                                       text: eventType.description)
        report(messaging: messaging)
    }
    
    func startScan() {
        reportBleEvent(eventType: .startScan)
        
    }
    
    func stopScan() {
        reportBleEvent(eventType: .stopScan)
        
    }
    
    func startConnectionProcedure() {
        reportBleEvent(eventType: .startConnectionProcedure)
    }
    
    func discoveredDevice(name: String) {
        reportBleEvent(eventType: .discoveredDevice(name))
    }
    
    func connectedToDevice(deviceUUID: String) {
        reportBleEvent(eventType: .connectedToDevice)
    }
    
    func scanWithNoResults() {
        reportBleEvent(eventType: .scanDeviceEndedWithNoResults)
    }
    
    func discoveredServices(text: String) {
        reportBleEvent(eventType: .discoveredServices(text))
    }
    
    func discoveredCharacteristics(text: String) {
        reportBleEvent(eventType: .discoveredCharacteristics(text))
    }
    
    func registeredToNotifications() {
        reportBleEvent(eventType: .registeredToNotifications)
    }
    
    func forgetDevice(deviceUUID: String) {
        reportBleEvent(eventType: .forgetDevice)
    }
}

extension SDKMessagingReport: BleCommandsToCapDelegate {

    func reportBleLog(logs: [CapEventEntry]) {
        for log in logs {
            let messaging = BleEventReport(type: .capLogs(text: "log_\(log.getReportName())"),
                                           deviceId: macAddress,
                                           data: log.dataString,
                                           text: log.getEventText(),
                                           timestamp: Int(log.timestamp))
            report(messaging: messaging)
            DDLogInfo("log-\(log.opCodeStr)")
        }
    }
    
    
    func onEventRecived(event: CapResponseAPI, lastCommand: CapCommandAPI?) {
        let inText = event.rawValue.binary
        let outText = lastCommand?.opCode.binary ?? ""
        let payload = lastCommand?.payloadString ?? ""
        print("=================Payload \(payload)")
        let messaging = BleEventReport(type: .capEvent(text: "response_\(event.typeName)"),
                                       deviceId: macAddress,
                                       data: "in:\(inText) out:\(payload)",
                                       text: "in:\(inText) out:\(outText)")
        report(messaging: messaging)
    }
    
    func onEventSend(command: CapCommandAPI, rowData: Data) {
        let messaging = BleEventReport(type: .capCommand(name: "send_\(command.typeName)",
                                       time: 0),
                                       deviceId: macAddress,
                                       data: rowData.map{"\($0)"},
                                       text: command.eventText)
        report(messaging: messaging)
    }
}


extension WIOCapDataProcessor {
    
    func reportEvent(events: [CapEventEntry]) {
        WIOApp.shared.networkManager.sdkMessaging.reportBleLog(logs: events)
    }
}

extension CapEventEntry {
    var dataString: String {
        //return eventData?.map{$0.binary}.joined(separator: ",") ?? "0"
        return "\(value),\(stdDeviation)"
    }
}

extension Data {
    var hex: String {
        var hexString = ""
        for byte in self {
            hexString += String(format: "%02X", byte)
        }

        return hexString
    }
}

extension CapCommandAPI {
    var payloadString: String {
        return payload?.map{"\($0)"}.joined(separator: ",") ?? "0"
    }
}
