import Foundation
import WaterIOBleKit
import WaterIONetwork

extension SDKMessagingReport {
    
    var macAddress: String  {
        if let mac = CurrentDevice.currentDevice()?.MACAddress {
            return mac
        }
        return CurrentDevice.currentDevice()?.UUID ?? ""
    }
    
    var appType: String {
        return WIOApp.shared.appType
    }
    
    var appToken: String {
        return WIOApp.shared.appToken
    }
    
    var userId: Int? {
        return UserCredentialStore().userCredentials?.userId
    }
    
    //TODO list
    //UTC
}
