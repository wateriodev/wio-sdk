//
//  SDKGenerateData.swift
//  WioSDK
//
//  Created by israel on 03/05/2020.
//

import Foundation
import WaterIONetwork

final class SDKGenerateData {
    private let appType = WIOApp.shared.appType
    private let appToken = WIOApp.shared.appToken
    let analyitics = WIOApp.shared.networkManager.sdkMessaging
    public init() {}

    public func generateAll() {
        genarateBleError()
        genarateNetworkError()
        generateAtt()
        generateBleEvents()
    }
}


extension SDKGenerateData {
    public func genarateBleError() {
        for event in BleErrorType.all {
            analyitics.reportBleError(errorType: event)
        }
    }

    public func genarateNetworkError() {
        let url = URL(string: "https://google.com")
        analyitics.serverError(error: .emptyResponse(url))
        analyitics.serverError(error: .emptyResponse(url))
        analyitics.serverError(error: .notAuthenticated(url))
        analyitics.serverError(error: .other(url))
        analyitics.serverError(error: .parsingFailed(url))
        analyitics.serverError(error: .serverError(statusCode: 1000, url))
        analyitics.serverError(error: .emptyResponse(url))
        analyitics.serverError(error: .timeout(url))
        analyitics.serverError(error: .networkError(url))
    }
}

extension SDKGenerateData {
    public func generateAtt() {
        guard let appType = WIOApp.shared.appType,
            let appToken = WIOApp.shared.appToken,
            let userId = UserCredentialStore().userCredentials?.userId else {
                return
        }
        let messaging = AttributesReport(appType: appType,
                                        appToken: appToken,
                                        uid: userId,
                                        isPermissionBluetoothIos: false,
                                        isPermissionNotification: false,
                                        permissionGeolocation: "true",
                                        isBluetoothOn: true,
                                        isIbeaconOnIos: true,
                                        triggerEvent: "")
        analyitics.report(messaging: messaging)
    }
}

extension SDKGenerateData {

    public func generateBleEvents() {
        for event in BleEventType.all {
            analyitics.reportBleEvent(eventType: event)
        }
    }

}
