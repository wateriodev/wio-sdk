import Foundation
import WaterIONetwork
import WaterIOBleKit
import CocoaLumberjack

final class SDKMessagingReport {
    
    private let analyticsManager: FirebaseClient
    private let apiManager: WaterIOClient
    private let logFile = LogFile()
    private var errorReport: [String: Bool] = [:]
    private let logsHelper = LogHelper()
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    var isNotificatinoPermission: Bool = false

    
    public init(apiManager: WaterIOClient, networkManager: FirebaseClient) {
        self.analyticsManager = networkManager
        self.apiManager = apiManager
        startLog()
        startListenToBleEevnt()
    }
    
    func startListenToBleEevnt() {
        WIOApp.shared.bleMenegaer.analyticsDelegates = self
    }
    
    func start() {
        WIONotificationHelper.isAuthorized { [weak self] isAccept in
            self?.isNotificatinoPermission = isAccept
        }
        
//        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
//            self.reportAttribut(triggerEvent: "START_APP")
//        }
    }
    
    func report(messaging: SdkMessaging) {
        let json = messaging.toJSON()
        if messaging.reportType == .error {
            guard let errorType = json["event_type"] as? String else {
                return
            }
            if errorReport[errorType] == true {
                return
            }
            
            errorReport[errorType] = true
        }
        
        logFile.save(json: json)
        
        if messaging.logLevel != .High {
            return
        }
        
        //DDLogInfo("======== HIGH: \(json) ===== ")
        switch messaging.reportType {
        case .error:
            analyticsManager.reportSDKErrors(json: json)
        case .event:
            analyticsManager.reportSDKBLEConnectionEvents(json: json)
        case .attribut:
            analyticsManager.reportSDKAttributes(json: json)
        }
    }
}

extension SDKMessagingReport {
    
    
    
    // All Data from CocoaLumberjack
    func sendLogfile() {
        var fullDateFormat: String {
            let timeFormat = DateFormatter()
            timeFormat.dateFormat = "yyyy-MM-dd HH_mm_ss"
            return timeFormat.string(from: Date())
        }
        
        logsHelper.getLogsFiles { [weak self] files in
            guard let files = files else {
                return
            }
            
            for log in files {
                let context = String(decoding: log.data, as: UTF8.self)
                self?.sendFileToServer(path: log.path, fileName: fullDateFormat, context: context)
            }
        }
    }
    
    // Messaging data
    func sendLog(isSendAll: Bool = true) {
        sendLogfile()
        guard let logs =  logFile.getAllLogJson() else {
            return
        }
        
        
        if isSendAll {
            for log in logs {
                if let context = logFile.readFile(url: log.url) {
                    sendToServer(path: log.url, fileName: log.fileName, context: context)
                }
            }
        } else {
            for log in logs where logFile.isBeforeToday(fileName: log.fileName)  {
                if let context = logFile.readFile(url: log.url) {
                    sendToServer(path: log.url, fileName: log.fileName, context: context)
                }
            }
        }
    }
    
    private func sendToServer(path: URL, fileName: String, context: String) {
        let timeFormat = DateFormatter()
        timeFormat.dateFormat = "HH_mm_ss"
        let now = timeFormat.string(from: Date())
        DDLogInfo("sendToServer Log file send")
        apiManager.uploadLogFile(fileName: fileName + " \(now)", context: context)
        logFile.removeFile(path)
    }
    
    private func sendFileToServer(path: URL, fileName: String, context: String) {
        DDLogInfo("sendFileToServer Log file  send")
        apiManager.uploadLogFile(fileName: fileName, context: context)
        logFile.removeFile(path)
    }
    
    private func startLog() {
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(dateDidChange),
                                               name: NSNotification.Name.NSCalendarDayChanged,
                                               object: nil)
        sendLog(isSendAll: false)
    }
    
    @objc func dateDidChange() {
        sendLog()
    }
}


extension SDKMessagingReport {
   
    func reportAttribut(triggerEvent: String) {
        let bleManager = WIOApp.shared.bleMenegaer
        let isBleTrunOn = bleManager?.managerState == .poweredOn
        let isAuthorized = bleManager?.managerState == .unauthorized
        let locationStatus = WIOLocationService.shared.authorizationStatus.description
        let isBeaconMonitor = WIOBeaconController.shared.isMonitoring
        
        let messaging = AttributesReport(appType: self.appType,
                                         appToken: self.appToken,
                                         uid: self.userId,
                                         isPermissionBluetoothIos: !isAuthorized,
                                         isPermissionNotification: isNotificatinoPermission,
                                         permissionGeolocation: locationStatus,
                                         isBluetoothOn: isBleTrunOn,
                                         isIbeaconOnIos: isBeaconMonitor,
                                         triggerEvent: triggerEvent)
        
        DDLogInfo(messaging.description)
        self.report(messaging: messaging)
    }
    
    
    func reportAttributWithoutBluetoothService(triggerEvent: String) {
        let messaging = AttributesReport(appType: self.appType,
                                         appToken: self.appToken,
                                         uid: self.userId,
                                         isPermissionBluetoothIos: false,
                                         isPermissionNotification: false,
                                         permissionGeolocation: "NO",
                                         isBluetoothOn: false,
                                         isIbeaconOnIos: false,
                                         triggerEvent: triggerEvent)
        DDLogInfo(messaging.description)
        self.report(messaging: messaging)
    }
}
