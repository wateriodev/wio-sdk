import Foundation
import WaterIONetwork
import CocoaLumberjack

public protocol WIOApiDelegate: class {
    func userIdUpdated(_ userId: Int, json: [String: Any])
}

public protocol WIOApiErrorDelegate: class {
    func serverError(error: WaterIONetwork.WebserviceError)
}

final class WIOApi: WIOApiType {
    
    var userId: Int? {
        return UserCredentialStore().userCredentials?.userId
    }
    
    private var maxEmailSend = 0
    private var maxTokenRequest = 0
    
    internal weak var delegate: WIOApiDelegate?
    internal weak var errorDelegate: WIOApiErrorDelegate?
    
    private let waterIOClient: WaterIOClient
    private var isAnonymous: Bool = false
    
    init(waterIOClient: WaterIOClient) {
        self.waterIOClient = waterIOClient
    }
    
    func start(appToken: String) {
        if userId == nil && appToken.contains("-access") {
            getServerAccess  { [weak self] result in
                switch result {
                case .success:
                    DDLogInfo("Server access success")
                case .error(let error):
                    DDLogError("Server access Error")
                    self?.reportError(error: error)
                }
            }
        }
    }
    
    public func getServerAccess(completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.getServerAccess(userStage: getUserStage()) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let data):
                me.isAnonymous = true
                me.updteUserId(data: data)
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func login(email: String, password: String, completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.login(email: email, password: password) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let data):
                me.updteUserId(data: data)
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func register(email: String, password: String, completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.register(email: email,
                               password: password,
                               isConfirmTerms: true,
                               isConfirmAdvertising: false,
                               userStage: getUserStage()) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success(let data):
                me.updteUserId(data: data)
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func buildHydrationGoal(completion: @escaping (Result<Int>) -> Void) {
        waterIOClient.requestBuildHydrationProfile { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func updateUserCredential(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.updateLogin(email: email, password: password) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func resetPassword(email: String, appType: String, completion: @escaping (Result<String>) -> Void) {
        waterIOClient.resetPassword(email: email, appType: appType)  { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func updateProfile(json: JSONDictionary, completion: @escaping (Result<String>) -> Void) {
        waterIOClient.updateProfile(json: json) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func sendMessage(id: String, text: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.sendMessage(id: id, text: text)  { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func updateMessage(id: String, status: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.updateMessage(id: id, status: status) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func updateLogin(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.updateLogin(email: email, password: password) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getAllMissingMessage(completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.getUnrecivedMessages { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getVitaminsList(completion: @escaping (Result<[[String: Any]]>) -> Void) {
        waterIOClient.getVitaminsList { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getUserScore(completion: @escaping (Result<[String: Any]>) -> Void) {
        waterIOClient.getUserScore { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    
    public func getBottleCalibration(_ name: String, completion: @escaping (Result<[[String : Any]]>) -> Void) {
        waterIOClient.getBottleCalibration(name) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    
    public func getCalibrationBottlesList(completion: @escaping (Result<[[String : Any]]>) -> Void) {
        waterIOClient.getCalibrationBottlesList { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    func getPrivacyURL(completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.getPrivacyURL { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    func getTermsAndConditionsURL(completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.getTermsAndConditionsURL { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getSystemConfigurations(completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.getSystemConfigurations { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    func sendEmail(error: Error, url: URL?, title: String) {
        if maxEmailSend >= 1 {
            return
        }
        
        maxEmailSend += 1
        
        //var json = DeviceInfoReport().toJSON()
        var json = AttributesReport(appType: WIOApp.shared.appType,
                                    appToken: WIOApp.shared.appToken,
                                    uid: userId,
                                    isPermissionBluetoothIos: false,
                                    isPermissionNotification: false,
                                    permissionGeolocation: "true",
                                    isBluetoothOn: true,
                                    isIbeaconOnIos: true,
                                    triggerEvent: "").toJSON()
        
        if let url = url {
            json["url"] = url.absoluteString
        }
        
        
        let description = "\(title) " + json.description
        
        waterIOClient.sendInternalNotification(subject: "Error \(title)", body: description) { result in
            switch result {
            case .success:
                break
            case .error:
                break
            }
        }
    }
    
    public func refreshToken(completion: @escaping (Result<[String: Any]>) -> Void) {
        if maxTokenRequest >= 3 {
            return
        }
        
        maxTokenRequest += 1
        waterIOClient.getFirebaseIdToken { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
}

extension WIOApi {
    func updteUserId(data: [String: Any]) {
        guard let credentials = UserCredentialStore().userCredentials else {
            return
        }
        
        self.refreshToken { [weak self] result  in
            guard let me = self else { return }
            switch result {
            case .success(let result):
                me.delegate?.userIdUpdated(credentials.userId, json: result)
            case .error(let result):
                DDLogError("[ERROR] updteUserId \(result)")
            }
        }
        
        notifyOnUserIdUpdate(userId: credentials.userId)
    }
    
    func reportError(error: Error) {
        errorDelegate?.serverError(error: error as! WebserviceError)
        DDLogError("Server Error \(error)")
        
//        guard let error = error as? WebserviceError else {
//            return
//        }
//
//        switch error {
//        case .parsingFailed(let url):
//            sendEmail(error: error, url: url, title: "Parsing Failed")
//        case .notAuthenticated(let url?):
//            sendEmail(error: error, url: url, title: "not Authenticated")
//        case .other:
//            //sendEmail(error: error, url: url, title: "Other")
//            break
//        case .serverError(statusCode: _, _):
//            //sendEmail(error: error, url: url, title: "Server Error")
//            break
//        case .emptyResponse(let url):
//            sendEmail(error: error, url: url, title: "Empty response")
//        case .timeout(_):
//            break
//        case .networkError(_):
//            break
//        case .notAuthenticated(.none):
//            break
//        }
    }
}

extension WIOApi {
    
    func notifyOnUserIdUpdate(userId: Int) {
        let name = Notification.Name(NotificationsConstance.UserIdDidUpdate)
        NotificationCenter.default.post(name: name, object: userId)
    }
}

//MARK: Alexa
extension WIOApi {
    
    public func isSkillEnable(completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.isSkillEnable { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getAuthCode(redirectUri: String, clientId: String, completion: @escaping (Result<String>) -> Void) {
        
        waterIOClient.getAuthCode(redirectUri: redirectUri,
                                  clientId: clientId) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func updateAlexaUser(json: [String: Any], completion: @escaping (Result<[String : Any]>) -> Void) {
        waterIOClient.updateAlexaUser(json: json) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
}


extension WIOApi {
    func getUserStage() -> String {
        let app = UIApplication.shared
        let appScheme = "wio-utility://"
        if let url = URL(string: appScheme), app.canOpenURL(url) {
            print("App is install and can be opened")
            return "TEST"
        }
        return "PROD"
    }
}

extension WIOApi {
    public func getQuotes(completion: @escaping ((Result<String>) -> Void)) {
        waterIOClient.getQuotes { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getCalendarDailyGoal(date: String,
                                     timeOffset: String,
                                     completion: @escaping ((Result<[[String: Any]]>) -> Void)) {
        waterIOClient.getCalendarDailyGoal(date: date, timeOffset: timeOffset) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func getDailyHydration(date: String,
                                  timeOffset: String,
                                  completion: @escaping ((Result<[String: Any]>) -> Void)) {
        waterIOClient.getDailyHydration(date: date, timeOffset: timeOffset) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    
    
    
    public func setNotificationReminder(type: String,
                                        interval: String,
                                        completion: @escaping ((Result<[String: Any]>) -> Void)) {
        waterIOClient.setNotificationReminder(type: type, interval: interval) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
    }
    
    public func setTimeRangeHydrationDay(startDay: String,
                                         endDay: String,
                                         completion: @escaping ((Result<[String: Any]>) -> Void)) {
        waterIOClient.setTimeRangeHydrationDay(startDay: startDay, endDay: endDay) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
        
    }
    
    
    public func setManualDailyGoal(isManual: Bool,
                                         goal: Int,
                                         completion: @escaping ((Result<[String: Any]>) -> Void)) {
        waterIOClient.setManualDailyGoal(isManual: isManual, goal: goal) { [weak self] result in
            guard let me = self else { return }
            switch result {
            case .success:
                completion(result)
            case .error(let error):
                me.reportError(error: error)
                completion(result)
            }
        }
        
    }
    
}
