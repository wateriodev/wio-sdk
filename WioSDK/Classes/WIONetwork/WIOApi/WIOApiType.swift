//
//  WIOApiType.swift
//  WioSDK
//
//  Created by israel on 27/04/2020.
//

import Foundation
import WaterIONetwork

public protocol WIOApiType {
    
    var userId: Int? { get }
    
    func isSkillEnable(completion: @escaping (Result<[String : Any]>) -> Void)
    func getAuthCode(redirectUri: String, clientId: String, completion: @escaping (Result<String>) -> Void)
    func updateAlexaUser(json: [String: Any], completion: @escaping (Result<[String : Any]>) -> Void)
    
    
    func getServerAccess(completion: @escaping (Result<[String : Any]>) -> Void)
    func login(email: String, password: String, completion: @escaping (WaterIONetwork.Result<[String : Any]>) -> Void)
    func register(email: String, password: String, completion: @escaping (WaterIONetwork.Result<[String : Any]>) -> Void)
    func resetPassword(email: String, appType: String, completion: @escaping (WaterIONetwork.Result<String>) -> Void)
    func updateProfile(json: WaterIONetwork.JSONDictionary, completion: @escaping (WaterIONetwork.Result<String>) -> Void)
    func updateLogin(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void)
    
    func sendMessage(id: String, text: String, completion: @escaping (Result<[String: Any]>) -> Void)
    func updateMessage(id: String, status: String, completion: @escaping (Result<[String: Any]>) -> Void)
    func getAllMissingMessage(completion: @escaping (Result<[String: Any]>) -> Void)
    
    func buildHydrationGoal(completion: @escaping (WaterIONetwork.Result<Int>) -> Void)
    func getVitaminsList(completion: @escaping (Result<[[String: Any]]>) -> Void)
    func getUserScore(completion: @escaping (Result<[String: Any]>) -> Void)
    func getPrivacyURL(completion: @escaping (Result<[String: Any]>) -> Void)
    func getTermsAndConditionsURL(completion: @escaping (Result<[String: Any]>) -> Void)
    func getSystemConfigurations(completion: @escaping (Result<[String: Any]>) -> Void)
    
    func updateUserCredential(email: String, password: String, completion: @escaping (Result<[String: Any]>) -> Void)
    func refreshToken(completion: @escaping (Result<[String: Any]>) -> Void)
    
    func sendEmail(error: Error, url: URL?, title: String)
   
    func start(appToken: String)
    
    
    //FIXME: sould be private
    var delegate: WIOApiDelegate? { get set }
    var errorDelegate: WIOApiErrorDelegate? { get set }
    
    
    func getCalibrationBottlesList(completion: @escaping (Result<[[String : Any]]>) -> Void)
    func getBottleCalibration(_ name: String, completion: @escaping (Result<[[String : Any]]>) -> Void)
    
    
    ///
   func getQuotes(completion: @escaping ((Result<String>) -> Void))
   func getCalendarDailyGoal(date: String,
                             timeOffset: String,
                             completion: @escaping ((Result<[[String: Any]]>) -> Void))
   func getDailyHydration(date: String,
                          timeOffset: String,
                          completion: @escaping ((Result<[String: Any]>) -> Void))
   func setNotificationReminder(type: String,
                                interval: String,
                                completion: @escaping ((Result<[String: Any]>) -> Void))
   func setTimeRangeHydrationDay(startDay: String,
                                         endDay: String,
                                         completion: @escaping ((Result<[String: Any]>) -> Void))
    
    func setManualDailyGoal(isManual: Bool, goal: Int,
                                        completion: @escaping ((Result<[String: Any]>) -> Void))
}
