import Foundation
import WaterIONetwork

public class WIOAnalytics {

    public let analytics: FirebaseClient
    
    public init(analytics: FirebaseClient) {
        self.analytics = analytics
    }
}

extension WIOAnalytics: AppTrackerType {
    
    public func reportToUserlog(json: [String : Any]) {
        analytics.reportToUserlog(json: json)
    }

    public func openCap(timestamp: Int) {
        analytics.openCap(timestamp: timestamp)
    }

    public func closeCap(timestamp: Int) {
        analytics.closeCap(timestamp: timestamp)
    }

    public func waterMeasure(timestamp: Int, distance: Int, std: Int, amount: Int, source: String) {
        analytics.waterMeasure(timestamp: timestamp, distance: distance, std: std, amount: amount, source: source)
    }

    public func capMarried(timestamp: Int, additional_data: String) {
        analytics.capMarried(timestamp: timestamp, additional_data: additional_data)
    }

    public func scanningCap() {
        analytics.scanningCap()
    }

    public func newAttachedCap(capVersion: String) {
        analytics.newAttachedCap(capVersion: capVersion)
    }

    public func capReminder(timestamp: Int, source: String) {
        analytics.capReminder(timestamp: timestamp, source: source)
    }

    public func appEnterForeground() {
        analytics.appEnterForeground()
    }

    public func appEnterBackground() {
        analytics.appEnterBackground()
    }

    public func appScreen(_ screenName: String) {
       analytics.appScreen(screenName)
    }

    public func closeApp() {
        analytics.closeApp()
    }

    public func startApp() {
        analytics.startApp()
    }

    public func coachingZone(category: String) {
        analytics.coachingZone(category: category)
    }

    public func finishWizard() {
        analytics.finishWizard()
    }

    public func loginFailed() {
        analytics.loginFailed()
    }

    public func login() {
       analytics.login()
    }

    public func logout() {
        analytics.logout()
    }

    public func selectBottle(amount: Int, bottleName: String) {
        analytics.selectBottle(amount: amount, bottleName: bottleName)
    }

    public func removeBottleSelection(amount: Int, bottleName: String) {
     analytics.removeBottleSelection(amount: amount, bottleName: bottleName)
    }

    public func personalProfileUpdated() {
        analytics.personalProfileUpdated()
    }

    public func physicalProfileUpdated() {
        analytics.physicalProfileUpdated()
    }

    public func profilePictureChanged() {
        analytics.profilePictureChanged()
    }

    public func addWeeklySportEvent(amount: Int, duration: String, sportType: String, days: String) {
        analytics.addWeeklySportEvent(amount: amount, duration: duration, sportType: sportType, days: days)
    }

    public func removeWeeklySportEvent(amount: Int, duration: String, sportType: String, days: String) {
        analytics.removeWeeklySportEvent(amount: amount, duration: duration, sportType: sportType, days: days)
    }

    public func addSportEvent(amount: Int, duration: String, sportType: String, additional_data: String) {
        analytics.addSportEvent(amount: amount, duration: duration, sportType: sportType, additional_data: additional_data)
    }

    public func removeSportEvent(amount: Int, duration: String, sportType: String, additional_data: String) {
        analytics.removeSportEvent(amount: amount, duration: duration, sportType: sportType, additional_data: additional_data)
    }

    public func userHydrate(timestamp: Int, drinkType: String, source: String, data: Int, additionalData: String?) {
        analytics.userHydrate(timestamp: timestamp, drinkType: drinkType, source: source, data: data, additionalData: additionalData)
    }

    public func removeHydration(type: String, data: Int, additionalData: String?) {
        analytics.removeHydration(type: type, data: data, additionalData: additionalData)
    }

    public func notificationIsKeepAliveReceived(isBackground: Bool) {
      analytics.notificationIsKeepAliveReceived(isBackground: isBackground)
    }

    public func hydrated(timestamp: Int, amount: Int, levelStart: Int, levelEnd: Int, timeSpan: Int, source: String, drinkType: String) {
     analytics.hydrated(timestamp: timestamp, amount: amount, levelStart: levelStart, levelEnd: levelEnd, timeSpan: timeSpan, source: source, drinkType: drinkType)
    }

    public func supportMessage(message: String) {
        analytics.supportMessage(message: message)
    }

    public func clickOnUpdate() {
        analytics.clickOnUpdate()
    }

    public func startFota() {
        analytics.startFota()
    }

    public func endFota() {
        analytics.endFota()
    }

    public func afterFotaCap() {
        analytics.afterFotaCap()
    }

    public func doseTaken(timestamp: Int, source: SourceType) {
        analytics.doseTaken(timestamp: timestamp, source: source)
    }

    public func setReminder(hour: Int, minutes: Int) {
        analytics.setReminder(hour: hour, minutes: minutes)
    }

    public func dosageAmount(amount: Int) {
        analytics.dosageAmount(amount: amount)
    }

    public func vitaminType(vitaminType: String) {
        analytics.vitaminType(vitaminType: vitaminType)
    }

    public func setReminder(reminderTime: String) {
        analytics.setReminder(reminderTime: reminderTime)
    }

    public func setCapReminder(reminderTime: String) {
        analytics.setCapReminder(reminderTime: reminderTime)
    }

    public func setMoodRating(mood: MoodRating) {
        analytics.setMoodRating(mood: mood)
    }

    public func notificationReminder(time: String) {
        analytics.notificationReminder(time: time)
    }

    public func preReminder(timestamp: Int) {
        analytics.preReminder(timestamp: timestamp)
    }

    public func registerWithoutCap() {
        analytics.registerWithoutCap()
    }

    public func notificationRecived(action: String, body: String, messageCode: String?, messgaeType: String, title: String, path: String) {
        analytics.notificationRecived(action: action, body: body, messageCode: messageCode, messgaeType: messgaeType, title: title, path: path)

    }

    public func setLocation(lat: Double, lon: Double, source: String) {
        analytics.setLocation(lat: lat, lon: lon, source: source)
    }

    public func reportSDKAttributes(json: [String : Any]) {
        analytics.reportSDKAttributes(json: json)
    }

    public func reportSDKErrors(json: [String : Any]) {
        analytics.reportSDKErrors(json: json)
    }

    public func reportSDKBLEConnectionEvents(json: [String : Any]) {
        analytics.reportSDKBLEConnectionEvents(json: json)
    }
}
