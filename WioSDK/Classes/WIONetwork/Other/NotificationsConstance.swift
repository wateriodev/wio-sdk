//
//  NotificationsConstance.swift
//  WioSDK
//
//  Created by israel on 15/05/2020.
//

import Foundation

struct NotificationsConstance {
    static let UserIdDidUpdate = "network.userid.updated"
}
