//READ more: https://github.com/d7laungani/DLLocalNotifications
import CocoaLumberjack

public class WIOLocalNotification: NSObject {
    
    public func scheduleCategories(categories: [WIONotificationCategory]) {
        var notificationCategories = Set<UNNotificationCategory>()
        for category in categories {
            guard let categoryInstance = category.categoryInstance else { continue }
            notificationCategories.insert(categoryInstance)
        }
        UNUserNotificationCenter.current().setNotificationCategories(notificationCategories)
    }
    
    public func scheduleNotification(notification: WIONotification) {
        let content = UNMutableNotificationContent()
        content.title = notification.alertTitle ?? ""
        content.body = notification.alertBody ?? ""
        content.sound = .default
        
        if let userInfo = notification.userInfo {
            content.userInfo = userInfo
        }
        //content.badge = NSNumber(value: UIApplication.shared.applicationIconBadgeNumber + 1)
        if let category = notification.category {
            content.categoryIdentifier = category
        }
        
        let trigger = UNCalendarNotificationTrigger(dateMatching:
            convertToNotificationDateComponent(notification: notification,
                                               repeatInterval: notification.repeatInterval),
                                                    
                                                    repeats: notification.repeats)
        
        let request = UNNotificationRequest(identifier: notification.identifier!, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            if let error = error {
                DDLogInfo("scheduleNotification error: \(String(describing: error))!")
            } else {
                DDLogInfo("scheduleNotification succeeded: \(request.description)")
            }
        }
    }
    
    public func scheduleNotificationTimeInterval(notification: WIONotification, timeInterval: TimeInterval) {
        let content = UNMutableNotificationContent()
        content.title = notification.alertTitle ?? ""
        content.body = notification.alertBody ?? ""
        content.sound = .default
        
        if let userInfo = notification.userInfo {
            content.userInfo = userInfo
        }
        //content.badge = NSNumber(value: UIApplication.shared.applicationIconBadgeNumber + 1)
        if let category = notification.category {
            content.categoryIdentifier = category
        }
        
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: timeInterval, repeats: true)
        let request = UNNotificationRequest(identifier: notification.identifier!, content: content, trigger: trigger)
        let center = UNUserNotificationCenter.current()
        center.add(request) { (error) in
            if let error = error {
                DDLogInfo("scheduleNotificationTimeInterval error: \(String(describing: error))!")
            } else {
                DDLogInfo("scheduleNotificationTimeInterval succeeded! \(request.description)")
            }
        }
    }
    
    
    public func cancelAlllNotifications() {
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
    public func getPendingNotificationRequests(completionHandler: @escaping ([UNNotificationRequest]) -> Void) {
        UNUserNotificationCenter.current().getPendingNotificationRequests(completionHandler: completionHandler)
    }
    
    public func getPendingNotification(_ id: String, completionHandler: @escaping (UNNotificationRequest?) -> Void) {
        getPendingNotificationRequests { (notifications) in
            let notification = notifications.filter{$0.identifier == id}.first
            completionHandler(notification)
        }
    }
    
    public func cancelNotification(notification: WIONotification) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [(notification.localNotificationRequest?.identifier)!])
        notification.scheduled = false
    }
    
    public func cancelNotification(_ id: String) {
        UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id])
    }
    
    private func convertToNotificationDateComponent(notification: WIONotification, repeatInterval: RepeatingInterval   ) -> DateComponents {
        
        var newComponents = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second ], from: notification.fireDate!)
        
        if repeatInterval != .none {
            
            switch repeatInterval {
            case .minute:
                newComponents = Calendar.current.dateComponents([ .second], from: notification.fireDate!)
            case .hourly:
                newComponents = Calendar.current.dateComponents([ .minute], from: notification.fireDate!)
            case .daily:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: notification.fireDate!)
            case .weekly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .second, .weekday, .nanosecond], from: notification.fireDate!)
            case .monthly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .day], from: notification.fireDate!)
            case .yearly:
                newComponents = Calendar.current.dateComponents([.hour, .minute, .day, .month], from: notification.fireDate!)
            default:
                break
            }
        }
        
        return newComponents
    }
}
