import Foundation

import Foundation
import UserNotifications

public class WIONotificationCategory {

    // Holds the actions you want available for this category type
    private var actions: [UNNotificationAction]?

    // Hold the actual cateogry
    internal var categoryInstance: UNNotificationCategory?

    // Holds the identifier of the category type
    var identifier: String

    public init (categoryIdentifier: String) {
        identifier = categoryIdentifier
        actions = [UNNotificationAction]()
    }

    public func addActionButton(identifier: String?, title: String?) {

        let action = UNNotificationAction(identifier: identifier!,
                                          title: title!,
                                          options:  [UNNotificationActionOptions(rawValue: 0)])
        
        actions?.append(action)
        categoryInstance = UNNotificationCategory(identifier: self.identifier,
                                                  actions: self.actions!,
                                                  intentIdentifiers: [], options: [])
        
    }
}

