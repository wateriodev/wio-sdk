//
//  WIOPushCategory.swift
//  WioSDK
//
//  Created by israel on 10/12/2019.
//

import Foundation

public enum WIOPushCategoryType: String {
    case unknow = "unknow"
    case newChatMessage = "CHAT_NEW_MESSAGE"
    case pushWithButton = "PUSH_WITH_BUTTONS"
    
    //Buttons Id
    static let okButtonId = "push.action.ok"
    static let cancelButtonId = "push.action.cancel"
}

public struct WIOPushCategory {
    //FIXME: now only 1 category support, category ID overide the second one
    public init(){}
    func setupAllCategories() {
        setupYesNoCategory()
        //setupSendMessageCategory()
    }
    
    func setupYesNoCategory() {
        let scheduler = WIOLocalNotification()
        let okButtonId = WIOPushCategoryType.okButtonId
        let cancelButtonId =  WIOPushCategoryType.cancelButtonId
        let category = WIONotificationCategory(categoryIdentifier: WIOPushCategoryType.pushWithButton.rawValue)
        category.addActionButton(identifier: okButtonId, title: "Yes")
        category.addActionButton(identifier: cancelButtonId, title: "No")
        scheduler.scheduleCategories(categories: [category])
    }
    
    func setupSendMessageCategory() {
        let scheduler = WIOLocalNotification()
        let okButtonId = "send_Button"
        let cancelButtonId = "cancel_Button"
        let category = WIONotificationCategory(categoryIdentifier: WIOPushCategoryType.newChatMessage.rawValue)
        category.addActionButton(identifier: okButtonId, title: "Send")
        category.addActionButton(identifier: cancelButtonId, title: "Cancel")
        scheduler.scheduleCategories(categories: [category])
    }
}


