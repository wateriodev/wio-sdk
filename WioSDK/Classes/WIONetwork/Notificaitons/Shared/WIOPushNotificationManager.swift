//import UserNotifications
//import UIKit
//import CocoaLumberjack
//
//public protocol WIOPushNotificationManagerDelegate: class {
//    func onUserClickAction(_ notificationId: String, actionIdentifier: String)
//    func onPushReceived(userInfo: [AnyHashable : Any]?)
//}
//
//class WIOConstance {
//    static let onPushNotificaitonRecived = "push.onPushNotificaitonRecived"
//}
//
//public class WIOPushNotificationManager: NSObject {
//
//    public weak var delegate: WIOPushNotificationManagerDelegate?
//    private let firebasePushService = FirebasePushService()
//
//    public func start(userId: Int,
//                      configuration: PushConfiguration,
//                      launchOptions: [UIApplication.LaunchOptionsKey: Any]? = nil,
//                      onCompelte: (() -> Void)? = nil) {
//        WIONotificationHelper.askPermission { isAccept in
//            DDLogInfo("Notification isAccept:\(isAccept)")
//            if isAccept  {
//                self.configure(userId: userId, configuration: configuration)
//            }
//
//            if let onCompelte = onCompelte {
//                onCompelte()
//            }
//        }
//    }
//
//    func configure(userId: Int, configuration: PushConfiguration) {)
//        firebasePushService.startFirebase(option: configuration)
//        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
//            self.firebasePushService.updateUid(userId: userId)
//        }
//        WIOPushConfiguration().setupAllCategories()
//    }
//}
//
//public extension WIOPushNotificationManager {
//
//    func onDidRegisterForRemoteNotifications(withDeviceToken token: Data?) {
//        guard let token = token else {
//            fatalError("Invalid Device Token")
//        }
//        firebasePushService.onDidRegisterForRemoteNotifications(token: token)
//    }
//
//    func onDidFailToRegisterForRemoteNotificationsWithError(_ error: Error?) {
//        DDLogInfo("Invalid Device Token \(String(describing: error))")
//    }
//
//    func onUserClickAction(_ center: UNUserNotificationCenter, didReceive response: UNNotificationResponse) {
//        let userInfo = response.notification.request.content.userInfo
//        let notificationId = response.notification.request.content.categoryIdentifier
//        routePush(category: notificationId, userInfo: userInfo)
//        firebasePushService.onPushReceived(userInfo: userInfo, onStart: false)
//        delegate?.onUserClickAction(notificationId, actionIdentifier: response.actionIdentifier)
//        self.delegate?.onPushReceived(userInfo: userInfo)
//        DDLogInfo("user click Action \(response.actionIdentifier)")
//    }
//
//    func onPushReceived(userInfo: [AnyHashable : Any]?, onStart: Bool) {
//        firebasePushService.onPushReceived(userInfo: userInfo, onStart: onStart)
//        self.delegate?.onPushReceived(userInfo: userInfo)
//        if let dics = userInfo as? [String: Any],
//            let apn = dics["aps"] as? [String: Any],
//            let category = apn["category"] as? String {
//            routePush(category: category, userInfo: userInfo)
//        }
//    }
//
//    private func routePush(category: String, userInfo: [AnyHashable : Any]?) {
//        let state = WIOPushCategory(rawValue: category) ?? .unknow
//        switch state {
//        case .unknow:
//            return
//        case .newChatMessage:
//            let notificationName = WIOConstance.onPushNotificaitonRecived
//            NotificationCenter.default.post(name: Notification.Name(notificationName), object: userInfo)
//        case .pushWithButton:
//            let notificationName = WIOConstance.onPushNotificaitonRecived
//            NotificationCenter.default.post(name: Notification.Name(notificationName), object: userInfo)
//        }
//    }
//}


