import Foundation
import CocoaLumberjack

class WIONotificationHelper {
    
    static func isAuthorized(completionHandler: @escaping (Bool) -> Void) {
        #if DEBUG && (arch(i386) || arch(x86_64)) && os(iOS)
        DDLogInfo("always authorized on simulator")
        completionHandler(true)
        #else
        UNUserNotificationCenter.current().getNotificationSettings(completionHandler: { settings in
            DispatchQueue.main.async {
                completionHandler(settings.authorizationStatus == .authorized)
            }
        })
        #endif
    }
    
    static func askPermission(completion: @escaping (Bool) -> Void) {
        let center = UNUserNotificationCenter.current()
        let completionHandler: ((UNNotificationSettings) -> Void) =  { settings in
            if settings.authorizationStatus == .notDetermined {
                let authOptions: UNAuthorizationOptions = [.alert, .badge, .sound]
                center.requestAuthorization(options: authOptions, completionHandler: { granted, error in
                    if !granted {
                        DDLogInfo("un:Something went wrong")
                    }
                    DispatchQueue.main.async {
                        completion(granted)
                    }
                })
            } else {
                completion(settings.authorizationStatus == .authorized)
            }
        }
        
        #if DEBUG && (arch(i386) || arch(x86_64)) && os(iOS)
            DDLogInfo("skip notification authorization on simulator")
            completion(true)
        #else
            center.getNotificationSettings(completionHandler: { settings in
                DispatchQueue.main.async {
                    completionHandler(settings)
                }
            })
        #endif
    }
}
