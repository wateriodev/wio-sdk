//
//  FirebasePushServiceSetup.swift
//  WioSDK
//
//  Created by israel water-io on 07/02/2021.
//

import Foundation
import CocoaLumberjack

final class FirebasePushServiceSetup {
    
    private let firebaseWebToken: String
    private let hostAppId: String
    private var fcmToken: String?
    private var tokenWaiter: DispatchGroup?
    private let isSendbox: Bool

    init(firebaseWebToken: String, hostAppId: String, isSendbox: Bool) {
        self.isSendbox = isSendbox
        self.firebaseWebToken = firebaseWebToken
        self.hostAppId = hostAppId
    }
    
    public func registerToNotificaiton(_ deviceToken: Data) {
        let apnToken = deviceToken.map{ data in String(format: "%02.2hhx", data) }.joined()
        converAPNToFCM(apnToken)
        DDLogInfo("device token is: \(apnToken)")
    }
    
    public func subscribeToTopic(_ topic: String) {
        guard let FCMToken = self.fcmToken else {
            tokenWaiter?.notify(queue: .global(), execute: {
                self.subscribeToTopic(topic)
            })
            return
        }
        DDLogInfo("subscribeToTopic \(topic)")
        let parameters = "{ \"to\": \"/topics/\(topic)\", \"registration_tokens\": [\"\(FCMToken)\"]}"
        self.send("https://iid.googleapis.com/iid/v1:batchAdd", parameters: parameters) { response, error in
            if let error = error {
                DDLogError("[ERROR] \(error.localizedDescription)")
                return
            }
            DDLogInfo("[Succsess] your device is subscried to topic \(topic)")
        }
    }
    
    private func converAPNToFCM(_ apnToken: String) {
        let url = "https://iid.googleapis.com/iid/v1:batchImport"
        let parameters = "{ \"application\": \"\(hostAppId)\",\"sandbox\":\(isSendbox),    \"apns_tokens\":[ \"\(apnToken)\"    ]}"
        
        tokenWaiter = DispatchGroup()
        tokenWaiter?.enter()
        
        self.send(url, parameters: parameters) { [weak self] response, error in
            if let error = error {
                DDLogError("[ERROR] Firebase notification \(error.localizedDescription)")
                return
            }
            
            if let error = response?["error"] {
                DDLogError("[ERROR] Firebase notification \(error)")
                return
            }
            
            self?.tokenWaiter?.leave()
            if let result = response?["results"] as? [[String: AnyObject]] {
                if let token = result[0]["registration_token"] as? String {
                    self?.fcmToken = token
                }
            }
        }
    }
}


extension FirebasePushServiceSetup {
//    func send(_ url: String, parameters: String, onCompeleted: @escaping (Dictionary<String, Any>?, Error?) -> Void) {
//        let semaphore = DispatchSemaphore (value: 1)
//        let postData = parameters.data(using: .utf8)
//        var request = URLRequest(url: URL(string: url)!)
//        request.addValue("key= \(firebaseWebToken)", forHTTPHeaderField: "Authorization")
//        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
//        request.httpMethod = "POST"
//        request.httpBody = postData
//
//        let config = URLSessionConfiguration.default
//        let session = URLSession(configuration: config)
//
//
//        let task = session.dataTask(with: request) { data, response, error in
//
//            if let error = error {
//                DispatchQueue.main.async { onCompeleted(nil, error) }
//                return
//            }
//
//            guard let data = data else {
//                DispatchQueue.main.async { onCompeleted(nil, error) }
//                semaphore.signal()
//                return
//            }
//
//            if let response = String(data: data, encoding: .utf8), let dict = response.convertStringToDictionary() {
//                DispatchQueue.main.async { onCompeleted(dict, nil) }
//            } else {
//
//            }
//            semaphore.signal()
//        }
//        task.resume()
//        semaphore.wait()
//    }
    
    func send(_ url: String, parameters: String, onCompeleted: @escaping (Dictionary<String, Any>?, Error?) -> Void) {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        let session = URLSession(configuration: configuration)
        
        let url = URL(string: url)!
        let postData = parameters.data(using: .utf8)
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        request.addValue("key= \(firebaseWebToken)", forHTTPHeaderField: "Authorization")
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        request.httpBody = postData
        
        let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
            
            if let error = error {
                DispatchQueue.main.async { onCompeleted(nil, error) }
                return
            }
            
            guard let data = data else {
                DispatchQueue.main.async { onCompeleted(nil, error) }
                //semaphore.signal()
                return
            }
            
            if let response = String(data: data, encoding: .utf8), let dict = response.convertStringToDictionary() {
                DispatchQueue.main.async { onCompeleted(dict, nil) }
            } else {
                
            }
            
        })
        
        task.resume()
    }
}

extension String {
    func convertStringToDictionary() -> [String:AnyObject]? {
        if let data = self.data(using: .utf8) {
            do {
                let json = try JSONSerialization.jsonObject(with: data, options: .mutableContainers) as? [String:AnyObject]
                return json
            } catch {
                DDLogError("Something went wrong convertStringToDictionary")
            }
        }
        return nil
    }
}
