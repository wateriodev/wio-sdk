import Foundation
//import FirebaseMessaging
//import FirebaseCore
import CocoaLumberjack

final class FirebasePushService: NSObject  {
    internal var userId: Int?
    private var pushService: FirebasePushServiceSetup?
    private let isSendbox: Bool

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    init(isSendbox: Bool) {
        self.isSendbox = isSendbox
        super.init()
        self.registerToNotifications()
    }
    
    internal func startFirebase(option: PushConfiguration) {
        pushService = FirebasePushServiceSetup(firebaseWebToken: option.pushServerKey,
                                               hostAppId: option.hostBundleId, isSendbox: isSendbox)
    }
    
    internal func onPushReceived(userInfo: [AnyHashable : Any]?, onStart: Bool) {
//        guard let userInfo = userInfo else {
//            DDLogInfo("Invalid User Info")
//            return
//        }
        //Messaging.messaging().appDidReceiveMessage(userInfo)
    }
    
    internal func subscribeToTopics() {
        guard let appType =  WIOApp.shared.appType,
              let userId = userId else {
            DDLogError("failed to subscribe to topics")
            return
        }
        pushService?.subscribeToTopic("user-\(userId)")
        pushService?.subscribeToTopic("news")
        pushService?.subscribeToTopic("user-all")
        pushService?.subscribeToTopic("user-\(appType)")
    }
    
    internal func unSubscribeFromTopics(userId: Int) {
//        guard let appType =  WIOApp.shared.appType else {
//            DDLogInfo("failed to subscribe to topics")
//            return
//        }
    }
    
    //When user not implement Messaging.messaging().delegate = self
    //user should provide deviceToekn
    internal func onDidRegisterForRemoteNotifications(deviceToken: Data) {
        pushService?.registerToNotificaiton(deviceToken)
        self.subscribeToTopics()
    }
}

extension FirebasePushService {
    
    func registerToNotifications() {
        let name = Notification.Name(NotificationsConstance.UserIdDidUpdate)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateUid(notification:)),
                                               name: name,
                                               object: nil)
    }
    
    
    @objc private func updateUid(notification: NSNotification) {
        let userId = notification.object as! Int
        self.userId = userId
        subscribeToTopics()
    }
}
