import Foundation
//import FirebaseCore

public struct PushConfiguration {
    
    //private let appID: String
    internal let appType: String
    internal let pushServerKey: String
    internal let hostBundleId: String

    public init(appType: String, pushServerKey: String, hostBundleId: String) {
        self.appType = appType
        self.pushServerKey = pushServerKey
        self.hostBundleId = hostBundleId
    }
}
