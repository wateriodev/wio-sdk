import Foundation
import WaterIOBleKit

public class WIOApp {
    public static let shared = WIOApp()
    
    public var isDebugMode: Bool = false {
        didSet {
            self.log?.setupLogger(isDebugMode: isDebugMode)
        }
    }

    public var appToken: String!
    public var appType: String!
    public var bleMenegaer: WIOBleManager!
    public var networkManager: WIONetworkManager!

    private var log: LogSender?
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    init() {
        self.log = LogSender()
        self.log?.setupLogger(isDebugMode: isDebugMode)
        self.bleMenegaer = WIOBleManager(version: .hydration)
    }
       
    var capId: String? {
        if let macAdress = CurrentDevice.currentDevice()?.MACAddress {
            return macAdress
        }
        if let UUID = CurrentDevice.currentDevice()?.UUID {
            return UUID
        }
        return nil
    }
    
    public func start(appToken: String, appType: String) {
        self.appToken = appToken
        self.appType = appType
        self.networkManager = WIONetworkManager(appType: appType,
                                                appToken: appToken,
                                                deviceId: capId)
        networkManager.start()
        registerToCapConnection()
        startBle()
    }
    
    public func startNetwork(appToken: String, appType: String) {
        self.appToken = appToken
        self.appType = appType
        self.networkManager = WIONetworkManager(appType: appType,
                                                appToken: appToken,
                                                deviceId: capId)
        networkManager.start()
        registerToCapConnection()
        networkManager.sdkMessaging.reportAttributWithoutBluetoothService(triggerEvent: "START_APP")
    }
    
    public func startBle() {
        self.bleMenegaer.start()
        
        //FIXME - Wating to system return powered on
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) {
            self.networkManager.sdkMessaging.reportAttribut(triggerEvent: "START_APP")
        }
    }
    
    func registerToCapConnection() {
        NotificationCenter.default.addObserver(self, selector: #selector(onDeviceConnectedNotification),
                                                    name: NSNotification.Name(CapConnectionConstance.onDeviceConnected), object: nil)
    }
    public func showAllLogs() {
        self.log?.showSendLogs()
    }
    
    @objc func onDeviceConnectedNotification(notification: Notification) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if let capId = self.capId {
                self.networkManager.updateCapId(capId)
            }
        }
    }
}
