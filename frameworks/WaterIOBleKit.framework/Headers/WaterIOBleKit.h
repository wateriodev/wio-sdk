//
//  WaterIOBleKit.h
//  WaterIOBleKit
//
//  Created by israel on 15/08/2019.
//  Copyright © 2019 Water-io. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for WaterIOBleKit.
FOUNDATION_EXPORT double WaterIOBleKitVersionNumber;

//! Project version string for WaterIOBleKit.
FOUNDATION_EXPORT const unsigned char WaterIOBleKitVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <WaterIOBleKit/PublicHeader.h>
#import "BootLoaderServiceModel.h"
#import "Constants.h"
#import "CyCBManager.h"
#import "NSData+hexString.h"
#import "NSString+hex.h"
#import "OTAFileParser.h"
#import "PocketCreator.h"
#import "Utilities.h"
//#import "RZBPeripheral.h"
//#import "RZBCentralManager.h"
//#import "RZBScanInfo.h"
//
//#import "CBService+RZBExtension.h"
//#import "CBUUID+RZBPublic.h"
//#import "RZBBluetoothRepresentable.h"
//#import "RZBCentralManager+CommandHelper.h"
//#import "RZBCentralManager+Private.h"
//#import "RZBCommand.h"
//#import "RZBCommandDispatch.h"
//#import "RZBDeviceInfo.h"
//#import "RZBErrors.h"
//#import "RZBHeartRateMeasurement.h"
//#import "RZBLog+Private.h"
//#import "RZBLog.h"
//#import "RZBPeripheral+Private.h"
//#import "RZBPeripheral+RZBBattery.h"
//#import "RZBPeripheral+RZBHeartRate.h"
//#import "RZBUUIDPath.h"
//#import "RZBUserInteraction.h"
//#import "RZBluetooth.h"
